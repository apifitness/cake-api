$(document).ready(function () {
    var url_string = location.href; //window.location.href
    var url = new URL(url_string);
    $.ajax({
        url: baseUrl+'/objectif/get-current-objective-user.json',
        method:'GET',
        data:{
            id:id
        },
        success: function (data) {
            if(data.code !== undefined){
                $('#personnalObjectiveChart').after('<h3>Vous n\'avez pas défini d\'objectif pour cette semaine :( <a href="/objectif/nouveau">En définir un</a></h3>').hide();
                $('.ui.dimmer').transition('fade');
            }
            else{
                var currObj = data;
                var dateDebut = data[0]['DateDebut'];
                currObj = currObj[0]['objectif'];
                $.ajax({
                    url:baseUrl+'/user/get-data-objectif.json',
                    method:'GET',
                    data:{
                        id:id,
                      dateDebut: dateDebut
                    },
                    success: function (data) {
                        var totCount = JSON.parse(data);
                        totCount = totCount[0];
                        if(totCount < currObj){
                            currObj = currObj - totCount
                        }
                        else{
                            currObj = 0;
                        }
                        var ctx = $('#personnalObjectiveChart');
                        var graph = new Chart(ctx, {
                            type: 'doughnut',
                            data:{
                                datasets:[{
                                   data:[totCount, currObj],
                                    backgroundColor: [
                                        'rgba(0,187,0,1)',
                                        'rgba(187,0,0,1)'
                                    ]
                                }],
                                labels: ['Vos Pas ('+totCount+')', 'Restant ('+currObj+')']
                            }
                        });
                        $('.ui.dimmer').transition('fade');
                    }
                })

            }
        }
    })
    $.ajax({
        url:baseUrl+'/userstat/get-week-data',
        method:'GET',
        data:{
          id:id
        },
        success:function (data) {
            var groupData = JSON.parse(data);
            var dataChart = [];
            for(let dataTemp of groupData){
                var count = 0;
                for(let temp of dataTemp) count += temp.count;
                dataChart.push(count);
            }
            var chart = $('#DataWeekChartIndex');
            var graph = new Chart(chart, {
                type: 'bar',
                data: {
                    labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
                    datasets: [{
                        label: '# de Pas',
                        data: dataChart,
                        backgroundColor: [
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                        ],
                        borderColor: [
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    });
});