$(document).ready(function () {
    let groupDropdown = $('#groupDropdown');
    let clickable = true;
    if(tempMgid){
        groupDropdown.dropdown('set value', tempMgid);
    }
    groupDropdown.on('click', '.item', function () {
        if(!clickable) return;
        clickable = false;
        let drpdwn = $('#groupDropdown');
        let mgid = drpdwn.dropdown('get value');
        $.ajax({
            url:baseUrl+'/user/update-main-group',
            data:{
                mgid: mgid,
                uid: id
            },
            success: function (data) {
                clickable = true;
                data = JSON.parse(data);
                switch(parseInt(data.code)){
                    case 3:
                        alert("Erreur côté serveur");
                        break;
                    case 6:
                        alert("Vous n\'êtes pas dans ce groupe");
                        break;
                }
                clickable = true;
            }
        })
    });
});

