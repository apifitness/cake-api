/* YEET GLOBUL */
var groupChart;

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

$(document).ready(function () {
    var url_string = location.href; //window.location.href
    var url = new URL(url_string);
    var id = url.searchParams.get("id");

    /* Hide stuff at the beginning */
    $('#userdata').hide();
    $('#groupdata').hide();
    $('#allDataU').hide();
    $('#yearDataU').hide();
    $('#monthDataU').hide();
    $('#weekDataU').hide();

    /* Variables */
    var dataAllReady = false;
    var dataYearReady = false;
    var dataMonthReady = false;
    var dataWeekReady = true;
    var allWalks;
    var yearWalks;
    var monthWalks;

    /* AJAX REQUESTS */
    $.ajax({
        url: '/userstat/get-all-data-user.json',
        method: 'GET',
        data:{
          id:id
        },
        success: function (data) {
            allWalks = JSON.parse(data);
            var labels = [];
            var data = [];
            for (walk of allWalks) {
                data.push(walk.count);
                if(walk === allWalks[0] || walk === allWalks[allWalks.length-1]){
                    var tempDate = new Date(walk.date);

                    labels.push(tempDate.toString().split(' G')[0]);
                }
                else{
                    labels.push("");
                }
            }
            var chart = $('#allDataChart');

            var graph = new Chart(chart, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: '# de Pas',
                        data: data,
                        backgroundColor: [
                            'rgba(244,0,0,0.71)'
                        ],
                        borderColor: [
                            'rgba(255,0,0,1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes : [ {
                            gridLines : {
                                display : false
                            }
                        } ]
                    }
                }
            });
            dataAllReady = true;
        }
    }); // All data from current user

    var date = new Date();
    $.ajax({
        url: '/userstat/get-year-data-user.json',
        method: 'GET',
        data: {
            year: date.getYear() + 1900,
            id:id
        },
        success: function (data) {
            yearWalks = JSON.parse(data);
            var labels = [];
            var data = [];
            for (walkMonth of yearWalks) {
                var count = 0;
                for(walk of walkMonth){
                    count += walk.count;
                }
                data.push(count);
            }
            var chart = $('#DataYearChart');

            var graph = new Chart(chart, {
                type: 'bar',
                data: {
                    labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
                    datasets: [{
                        label: '# de Pas',
                        data: data,
                        backgroundColor: [
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                        ],
                        borderColor: [
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            dataYearReady = true;
        }
    }); // Yearly data from current user

    var month = date.getMonth() + 1;
    if (month < 10) month = "0" + month;
    $.ajax({
        url: 'userstat/get-data-month.json',
        method: 'GET',
        data: {
            year: date.getYear() + 1900,
            month: month,
            id:id
        },
        success: function (data) {
            monthWalks = JSON.parse(data);
            var data = [];
            var labels = [];
            for (walk of monthWalks) {
                data.push(walk.count);
                labels.push("");
            }
            var chart = $('#DataMonthChart');

            var graph = new Chart(chart, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: '# de Pas',
                        data: data,
                        backgroundColor: [
                            'rgba(244,0,0,0.71)'
                        ],
                        borderColor: [
                            'rgba(255,0,0,1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            dataMonthReady = true;
        }
    }); // Monthly

    var day = date.getDate();
    var dayN = date.getDay();
    $.ajax({
        url:'userstat/get-week-data.json',
        method:'GET',
        data:{
            year: date.getFullYear(),
            month: month,
            currDay:day,
            dayNo:dayN,
            id:id
        },
        success:function (data) {
            var groupData = JSON.parse(data);
            var dataChart = [];
            for(let dataTemp of groupData){
                var count = 0;
                for(let temp of dataTemp) count += temp.count;
                dataChart.push(count);
            }
            var chart = $('#DataWeekChart');
            var graph = new Chart(chart, {
                type: 'bar',
                data: {
                    labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
                    datasets: [{
                        label: '# de Pas',
                        data: dataChart,
                        backgroundColor: [
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                            'rgba(244,0,0,0.71)',
                        ],
                        borderColor: [
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                            'rgba(255,0,0,1)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    });

    /* SEMANTIC UI INIT -- DO NOT ERASE */

    $('.ui.dropdown').dropdown();

    /* ON CLICK LISTENERS */
    $('#selectedmode').on('click', '.item', function () {
       var mode = $('#modeselection').dropdown('get value');
       $('#defaultDisplay').hide();
       if(mode === "U"){
           $('#userdata').show();
           $('#groupdata').hide();
       }
       if(mode === "G"){
           $('#userdata').hide();
           $('#groupdata').show();
       }
    });

    $('#selectedtypeU').on('click', '.item', function () {
       var mode = $('#typeselectionU').dropdown('get value');
       if(mode === "G"){
            $('#allDataU').show();
            $('#yearDataU').hide();
            $('#monthDataU').hide();
            $('#weekDataU').hide();
       }
        if(mode === "Y"){
            $('#allDataU').hide();
            $('#yearDataU').show();
            $('#monthDataU').hide();
            $('#weekDataU').hide();
        }
        if(mode === "M"){
            $('#allDataU').hide();
            $('#yearDataU').hide();
            $('#monthDataU').show();
            $('#weekDataU').hide();
        }
        if(mode === "W"){
            $('#allDataU').hide();
            $('#yearDataU').hide();
            $('#monthDataU').hide();
            $('#weekDataU').show();
        }
    });


    $('#selectedgroup').on('click', '.item', function () {
        $('.ui.dimmer').transition('reset');
       var Gid = $('#groupselection').dropdown('get value');
       $.ajax({
           url: 'groupe/get-one.json',
           method: 'GET',
           data:{
               id: Gid
           },
           success: function (data) {
               var colors = ['rgba(125,0,0,1)', 'rgba(0,125,0,1)', 'rgba(0,0,125,1)', 'rgba(255,100,200,1)', 'rgba(255,181,0,1)']
               var groupData = data
               var dataChart = [];
               var i = 0;
               var labels = [];
               for(var user of groupData.user){
                   var label = "";
                   var total = 0;
                   for(var userstat of user.userstat){
                       total += userstat.count;
                   }
                   dataChart.push({
                       data:[total],
                       label:user.firstName,
                       backgroundColor: colors[i]});
                   i++;
                   labels.push(label);
               }
               var chart = $('#dataChartGroup');
               if(groupChart !== undefined) groupChart.destroy();
               groupChart = new Chart(chart, {
                   type: 'bar',
                   data: {
                       datasets: dataChart,
                       labels: [""]
                   },
                   options:{
                       scales:{
                           xAxes: [{
                               stacked:true
                           }],
                           yAxes: [{
                               stacked:true
                           }]
                       }
                   }
               });
               $('.ui.dimmer').transition('fade');
           },
           error:function (msg) {
               console.log(msg);
           }
       })
    });
});