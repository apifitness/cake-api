$(document).ready(function () {
    $('#connectForm').hide();
    $('#thirdPartUI').hide();
    $('#fourthPartU').hide();
    $('#errormess').hide();
    $('#succmessage').hide();
    $('#wristbandId').hide();
    $('#badmess').hide();
    $('#goodmess').hide();
    $('#inputDate').popup({on:'click'});

    showsecond();

    $('#clickMDPRESET').on('click', function (event) {
        location.href = '/reset-password'
    });

    $('.field.radio').on('click', 'input', showThird);
    $('#connectU').on('click', function (event) {
        $('#dimmerU').show();
        $('#mailU').parent().removeClass('error');
        $('#passU').parent().removeClass('error');
        event.preventDefault();
        $('#errormess').hide();
        $('#succmessage').hide();
       var mailU = $('#mailU').val();
       var passU = $('#passU').val();
        $.ajax({
            url: baseUrl+'/user/connect.json',
            method: 'POST',
            data: {
                mail: mailU,
                pass:sha256(passU)
            },
            beforeSend: function (xhr) { // Add this line
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },
            success: function(data){
                var code = data;
                switch (code.code) {
                    case "BAD PASSWORD":
                        $('#errormess').show();
                        $('#errorcontent').text("Mot de passe incorrect.");
                        $('#passU').parent().transition('shake');
                        $('#passU').parent().addClass('error');
                        break;
                    case "NOT EXISTING":
                        $('#errormess').show();
                        $('#errorcontent').text("E-mail inexistante dans nos fichiers.");
                        $('#mailU').parent().transition('shake');
                        $('#mailU').parent().addClass('error');
                        break;
                    default:
                        $('#succmessage').show();
                        setTimeout(function () {
                            window.location.href=baseUrl+"/user/connexion?email=" + mailU;
                        }, 1000);
                        break;
                }
                $('#dimmerU').hide();
            },
            error: function (err) {
                console.log(err);
            }
        });
    });


    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;


    $('.ui.dropdown').dropdown();
    $('#toInscr').on('click', toInscr);
    $('#toConn').on('click', toConn);
    $('.ui.checkbox').on('click', '#isWristband', function () {
       if($('.ui.checkbox #isWristband').is(':checked')){
            $('#wristbandId').show();
       }
       else{
           $('#wristbandId').hide();
       }
    });
    $('#formReg').on('submit', function (event) {
        $('#badmess').hide();
        $('#goodmess').hide();
       event.preventDefault();
       var formData = new FormData(document.getElementById('formReg'));
       var formInputs = $('#formReg input');
       for(var input of formInputs){
           if(input.value.trim() === "" && !(input.id === "")){
               if((input.id !== 'fnU' && input.id !== 'lnU' && input.id !== 'mailUI' && input.id !== 'passUI')){
               }
               else{
                   $('#errmess').text('Un champ n\'a pas été correctement rempli.');
                   $(input).parent().addClass('error').transition('shake');
                   $('#eerrmess').transition('reset');
                   $('#badmess').show();
                   return false;
               }
           }
       }
       formData.append('role', '0');
       formData.append('status', '0');
       formData.append('defaultScore', '0');
       formData.append('connectionType', '0');
       var tempDate = new Date();
       var toDate = tempDate.getUTCFullYear()+'-'+tempDate.getMonth()+'-'+tempDate.getUTCDate()+' '+tempDate.getUTCHours()+':'+tempDate.getUTCMinutes()+':'+tempDate.getUTCSeconds();
       formData.append('lastUpdate', toDate);
       formData.append('weight', $('#weight_dropdown').dropdown('get value'));
        formData.append('size', $('#height_dropdown').dropdown('get value'));
       $.ajax({
           url:baseUrl+'/register',
           method:'POST',
           data:formData,
           processData: false,
           contentType: false,
           success: function (data) {
               console.log(data);
                var response = JSON.parse(data);
                for(let key in response){
                    switch(key){
                        case "badmess":
                            $('#errmess').text(response[key]);
                            $('#badmess').show();
                            break;
                        case "goodmess":
                            $('#'+key).show();
                            window.location.href = baseUrl+'/displaydata';
                            break;
                    }
                }
           },
           error: function (err) {
               console.log(err);
           }
        });
    });
});

function showsecond(){
    $('#secondPart').transition('fade', '2s');
}

function showThird(){
    $('#thirdPart').show();
}

function toInscr(event){
    event.preventDefault;
    $('#thirdPartU').hide();
    $('#thirdPartUI').show();
}

function toConn(event) {
    event.preventDefault;
    $('#thirdPartU').show();
    $('#thirdPartUI').hide();
}