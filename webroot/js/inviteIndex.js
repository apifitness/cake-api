$(document).ready(function () {
   let user_id = id;
   let sent = $('#sentRequests');
   let recv = $('#receivedRequests');
   $.ajax({
      url: baseUrl+'/invitation/get-all-by-id.json',
      method:'GET',
      data:{
         id: user_id
      },
      success: function (data) {
         var invites = data;
         if(invites.recv.length === 0){
            recv.append('<tr><td>Vous n\'avez pas reçu d\'invitations.</td></tr>');
         }
         else{
            for(let received of invites.recv){
               console.log(received);
               recv.append('<tr><td>'+received.username+'</td><td>'+received.groupname+'</td><td id="g'+received.groupe_id+'i'+received.id+'"><button class="ui green button icon buttonaccept" type="button"><i class="check icon"></i></button><button class="ui button icon red buttonrefuse" type="button"><i class="close icon"></i></button></td></tr>');
            }
         }
         if(invites.sent.length === 0){
            sent.append('<tr><td>Vous n\'avez pas envoyé d\'invitations.</td></tr>');
         }
         else{
            for(let sentI of invites.sent){
               sent.append('<tr><td>'+sentI.username+'</td><td>'+sentI.groupname+'</td><td></td></tr>');
            }
         }
      },
      error: function (err) {
         recv.append('<tr><td>Vous n\'avez pas reçu d\'invitations.</td></tr>');
         sent.append('<tr><td>Vous n\'avez pas envoyé d\'invitations.</td></tr>');
      }
   });
   recv.on('click', '.buttonaccept', function () {
      let id = $(this).parent().attr('id');
      let uid = user_id;
      let gid = (id.split('g')[1]).split('i')[0];
      let iid = id.split('i')[1];
      ajaxJoin(uid, gid, iid, $(this).parent().parent());
   });
   recv.on('click', '.buttonrefuse', function () {
      let id = $(this).parent().attr('id');
      let iid = id.split('i')[1];
      ajaxRefuse(iid, $(this).parent().parent());
   });

   $('#addIcon').on('click', function () {
      window.location.href = baseUrl+"/invitation/creation";
   });
});

function ajaxJoin(uid, gid, iid, jqs) {
   $.ajax({
      url:baseUrl+'/groupe/join.json',
      method:'GET',
      data:{
         user_id:uid,
         group_id:gid,
         inv_id:iid
      },
      success:function () {
         jqs.remove();
      },
      error: console.log
   });
}

function ajaxRefuse(iid, jqs) {
   $.ajax({
      url:baseUrl+'/invitation/refuse.json',
      method:'GET',
      data:{
         inv_id:iid
      },
      success:function () {
         jqs.remove();
      },
      error: console.log
   });
}