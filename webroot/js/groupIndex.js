$(document).ready(function () {
    let uid = id;
    let groupTable = $('#groupTable');
    $.ajax({
        url:baseUrl+'/groupe/get-all.json',
        method:'GET',
        data:{
            id: uid
        },
        success: function (data) {
            let groups = data;
            if(groups){
                for(let group of groups){
                    groupTable.append('<div class="ui grid"><tr><div class="two column row"><td><div class="nine wide column"><p>' + group.nom + '</p><ul id="'+group.nom+'Members"></ul></div><div class="seven wide column"><button type="button" class="ui fluid button icon fright leaveGroup">Quitter le groupe <i class="ui share square icon"></i></div></div></button></td></tr>');
                    for(let user of group.user){
                        $('#'+group.nom+'Members').append('<li>'+user.firstName+' '+user.lastName+'</li>');
                    }
                }
            }
            else{
                groupTable.append('<tr><td>Vous n\'êtes dans aucun groupe :(</td></tr>');
            }
        },
        error: function (err) {
            console.log(err);
        }
    });

    $('#addIcon').on('click', function () {
        if(!$('.confirmCreation').length){
            $('#groupTable').append('<tr><td><div class="field"><div class="ui input"><input type="text" name="nom" placeholder="Nom du groupe" id="groupName"/></div><button class="ui button confirmCreation" type="button">SAUVEGARDER</button></div></td></tr>')
        }
    });

    groupTable.on('click', '.confirmCreation', function (event) {
        let nomGroupe = $('#groupName').val();
        $.ajax({
            url:baseUrl+'/groupe/create-new.json',
            method:'GET',
            data:{
                nom: nomGroupe,
                id: uid
            },
            success: function (res) {
                if(res.status !== "ERR"){
                    location.href = baseUrl+'/invitation/creation?gName='+res.status;
                }
            }
        });
        event.target.parentElement.parentElement.remove();
    });

    groupTable.on('click', '.leaveGroup', function (event) {
        let nomGroupe = event.target.parentElement.innerHTML.split('<')[0];
       $.ajax({
           url:baseUrl+'/groupe/leave',
           method:'GET',
           data:{
               nom: nomGroupe,
               id: uid
           },
           success: function (data) {
               event.target.parentElement.parentElement.remove();
           }
       })
    });

});