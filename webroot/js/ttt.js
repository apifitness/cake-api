const days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi','Samedi', 'Dimanche']; // Labels
const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
$(document).ready(function () {

   let date = new Date();
   let monthDate = new Date();
   let timeOut;

   //region First ajaxes + date
   updateDay(date);
   updateDayMonth(monthDate);
   weekDataAjax(date);
   if(mgid !== "") groupWeekDataAjax(date);
   monthDataAjax(monthDate);
   if(mgid !== "") groupMonthDataAjax(monthDate);
   //endregion

   // region onClick listeners
   $('.prevWeek').on('click', function () {
      date.setDate(date.getDate() - 7);
      updateDay(date);
      if(timeOut) clearTimeout(timeOut);
      timeOut = setTimeout(function () {
         weekDataAjax(date);
         if(mgid !== "") groupWeekDataAjax(date);
      }, 100);
   });
   $('.nextWeek').on('click', function () {
      date.setDate(date.getDate() + 7);
      updateDay(date);
      if (timeOut) clearTimeout(timeOut);
      timeOut = setTimeout(function () {
         weekDataAjax(date);
         if(mgid !== "") groupWeekDataAjax(date);
      }, 100);
   });

   $('.prevMonth').on('click', function () {
      monthDate.setMonth(monthDate.getMonth() - 1);
      updateDayMonth(monthDate);
      monthDataAjax(monthDate);
      if(mgid !== "") groupMonthDataAjax(monthDate);
   });

   $('.nextMonth').on('click', function () {
      monthDate.setMonth(monthDate.getMonth() + 1);
      updateDayMonth(monthDate);
      monthDataAjax(monthDate);
      if(mgid !== "") groupMonthDataAjax(monthDate);
   });

   setInterval(function() { swapColors() }, 1000);

   $('.ui.tabular').on('click', '.item', function (event) {
      let groupName = event.target.innerHTML.trim();
      if(groupName === "Données Mensuelles"){
         let chartMonthMobile = $('#chartMonthMobile');
         chartMonthMobile.CanvasJSChart(chartMonthMobile.CanvasJSChart().options);

         let chartMonthGroupMobile = $('#chartMonthGroupMobile');
         chartMonthGroupMobile.CanvasJSChart(chartMonthGroupMobile.CanvasJSChart().options);
      }
      else{
         let chartWeekMobile = $('#chartWeekMobile');
         chartWeekMobile.CanvasJSChart(chartWeekMobile.CanvasJSChart().options);
         let chartWeekGroupMobile = $('#chartWeekGroupMobile');
         chartWeekGroupMobile.CanvasJSChart(chartWeekGroupMobile.CanvasJSChart().options);
      }

   });

   //endregion
   $('.tabular.menu .item').tab();
});

//region Ajax functions

function groupWeekDataAjax(date) {
   let month = date.getMonth()+1;
   if(month < 10){
      month = "0"+month;
   }
   let day = date.getDate();
   let dayN = date.getDay();
   $.ajax({
      url: baseUrl+'/groupe/get-one.json',
      method: 'GET',
      data:{
         id: mgid
      },
      success: function (data) {
         $('.tabular.menu .item').tab();
         let groupName = data['nom'];
         $('#gName').text(groupName);
         let groupUsers = data['user'];
         let userData = [];
         for(let user of groupUsers) {
            $.ajax({
               url: baseUrl+'/userstat/get-week-data',
               data: {
                  year: date.getFullYear(),
                  month: month,
                  currDay: day,
                  dayNo: dayN,
                  id: user.id
               },
               success: function (data) {
                  let fullName = user['firstName'];
                  userData.push(JSON.parse(data));
                  userData[userData.length - 1]['username'] = fullName;
                  if (userData.length === groupUsers.length) {
                     let dayCount = [{dataPoints: []}, {dataPoints: []}, {dataPoints: []}, {dataPoints: []}, {dataPoints: []}, {dataPoints: []}, {dataPoints: []}];

                     for (let i = 0; i < 7; i++) {
                        let currD = dayCount[i];
                        currD.name = days[i];
                        currD.type = 'stackedColumn';
                     }
                     dayCount[6].indexLabel = "#total";
                     dayCount[6].indexLabelPlacement = "outside";
                     for (let dataTemp of userData) {
                        fullName = dataTemp.username;
                        let i = 0;
                        while (i < 7) {
                           if (dataTemp[i] !== undefined) {
                              let cnt = 0;
                              if (typeof dataTemp[i] !== typeof "") {
                                 for (let dataDay of dataTemp[i]) {
                                    cnt += dataDay.count;
                                 }
                              }
                              dayCount[i].dataPoints.push({y: cnt, label: fullName});
                           } else {
                              dayCount[i].dataPoints.push({y: 0, label: fullName});
                           }
                           i++;
                        }
                     }
                     let options = {
                        axisY: {
                           title: "",
                           tickLength: 0,
                           lineThickness: 0,
                           margin: 0,
                           gridThickness: 0,
                           valueFormatString: " "
                        },
                        title: {
                           text: "Répartition des pas du groupe " + groupName
                        },
                        data: dayCount,
                        dataPointMaxWidth: 60
                     };
                     let groupChartWeek = $('#groupChartWeekComputer');
                     let chartWeekGroupMobile = $('#chartWeekGroupMobile');
                     groupChartWeek.CanvasJSChart(options);
                     chartWeekGroupMobile.CanvasJSChart(options);
                  }
               }
            });
            let groupImagesWeekly = $('#groupImagesWeeklyComputer');
            let groupImagesWeeklyMobile = $('#groupImagesWeeklyMobile');
            if ($('#groupImagesWeeklyComputer img').length === groupUsers.length) groupImagesWeekly.empty();
            if($('#groupImagesWeeklyMobile img').length === groupUsers.length) groupImagesWeekly.empty();
            user['avatar'] = user['avatar'] === null ? getNoImageString() : user['avatar'];
            let toAppend = '<img src="' + user['avatar'] + '" alt="avatar utilisateur" class="ui image" height="auto" width="20%"/>';
            groupImagesWeeklyMobile.append(toAppend);
            groupImagesWeekly.append(toAppend);
            let padding = (50 - ((groupUsers.length - 1) * 5)) / groupUsers.length;
            padding += "%";
            groupImagesWeeklyMobile.css('margin-left', padding).css('margin-right', 100-padding)
            groupImagesWeekly.css('margin-left', padding).css('margin-right', 100-padding)
         }
      }
   });
}

function weekDataAjax(date) {
   let month = date.getMonth()+1;
   if(month < 10){
      month = "0"+month;
   }
   let day = date.getDate();
   let dayN = 6;
   if(date.toString() === new Date().toString()){
      dayN = date.getDay();
   }

   $.ajax({
      url: baseUrl+'/objectif/get-current-objective-user.json',
      method: 'GET',
      data:{
         id: id
      },
      success: function (data) {
         let dataObj = null;
         let arrayVal = [];
         if(data.code === undefined){
            let obj = data[0]['objectif'];
            for(let i = 1; i < 8; i++){
               arrayVal.push({x:i*10, y: obj, label: days[i-1]});
            }
            dataObj = {
               type: "line",
               name: "Objectif",
               showInLegend: "true",
               markerType: "none",
               color: "#0076ad",
               dataPoints: arrayVal
            }
         }
         $.ajax({
            url:baseUrl+'/userstat/get-week-data.json',
            method:'GET',
            data:{
               year: date.getFullYear(),
               month: month,
               currDay:day,
               dayNo:dayN,
               id:id
            },
            success:function (data) {
               let groupData = JSON.parse(data);
               let dataChart = [];
               let dataGroup = [];
               let i = 1;
               for (let dataTemp of groupData) {
                  let count = 0;
                  for (let temp of dataTemp) count += temp.count;
                  if(arrayVal.length){
                     let objDa = arrayVal[i - 1]['y'];
                     if (count < objDa) {
                        dataGroup.push({
                           x: i * 10,
                           y: count,
                           label: days[i - 1],
                           indexLabel: count.toString(),
                           color: '#007677',
                           legendMarkerColor:'#007677'
                        })
                     } else {
                        dataGroup.push({
                           x: i * 10,
                           y: count,
                           label: days[i - 1],
                           indexLabel: count.toString(),
                           color: '#00c800',
                           legendMarkerColor:'#00c800'
                        });
                     }
                  }
                  else{
                     dataGroup.push({
                        x: i * 10,
                        y: count,
                        label: days[i - 1],
                        indexLabel: count.toString(),
                        color: '#007677',
                        legendMarkerColor:'#007677'
                     });
                  }

                  i++;
               }
               while (i < 8) {
                  dataGroup.push({x: i * 10, y: 0, label: days[i - 1], indexLabel: '0'});
                  i++;
               }
               dataChart.push({
                  name:'# de pas',
                  showInLegend: "true",
                  type: "column",
                  dataPoints: dataGroup,
                  legendMarkerColor:'#00c800'
               });
               if (dataObj !== null) {
                  dataChart.push(dataObj);
               }

               let objWeek = $('#objWeek');
               let chartWeekMobile = $('#chartWeekMobile');
               let optn = {
                  axisY:{
                     title: "",
                     tickLength: 0,
                     lineThickness:0,
                     margin:0,
                     gridThickness: 0,
                     valueFormatString:" "
                  },
                  axisX:{
                     title: "",
                     tickLength: 0,
                     margin:0,
                     lineThickness:0,
                     valueFormatString:" " //comment this to show numeric values
                  },
                  title: {
                     text: "Mon objectif journalier"
                  },
                  data: dataChart,
                  dataPointMaxWidth: 60
               };
               objWeek.CanvasJSChart(optn);
               chartWeekMobile.CanvasJSChart(optn);
            }
         });
      }
   });
}

function monthDataAjax(date) {
   // get month of given date
   let month = date.getMonth()+1;
   if(month < 10){
      month = "0"+month;
   }
   // Ajax request
   $.ajax({
      url:baseUrl+'/userstat/get-data-monthly',
      method: "GET",
      data:{
         id:id,
         month: month,
         year: date.getFullYear()
      },
      success: function(data){
         // data = ['data'=>[Données de pas de l'utilisateur courant], 'dates'=>[beginDate]]
         let dataJSON = JSON.parse(data);
         let weekData = dataJSON.data;
         let tempWeek = new Date(dataJSON['beginDate']);
         let weeks = [];
         for(let i = 0; i < weekData.length; i++){
            weeks.push(formatDate(tempWeek));
            tempWeek.setDate(tempWeek.getDate() + 7);
         }
         let chartData = [];
         for(let i = 0; i < 7; i++){
            chartData.push([]);
            let j = 0;
            for(let dailyData of weekData){
               chartData[i].push({y: dailyData[i], label: weeks[j]});
               j++;
            }
         }
         let dataFinal = [];
         let i = 0;
         for(let weekDataPoints of chartData){
            dataFinal.push({
               type: "stackedColumn",
               name: days[i],
               showInLegend:"true",
               yValueFormatString: "#0 steps",
               dataPoints: weekDataPoints
            });
            i++;
         }
         dataFinal[dataFinal.length - 1].indexLabel="#total";
         dataFinal[dataFinal.length - 1].indexLabelPlacement="outside";
         let optn = {
            animationEnabled:false,
            toolTip: {
               shared: true,
               reversed: true
            },
            axisY:{
               title: "",
               tickLength: 0,
               lineThickness:0,
               margin:0,
               gridThickness: 0,
               valueFormatString:" "
            },
            axisX:{
               title: "",
               tickLength: 0,
               margin:0,
               lineThickness:0,
               valueFormatString:" " //comment this to show numeric values
            },
            title:{
               text:"Nombre de pas mensuel"
            },
            data: dataFinal,
            dataPointMaxWidth: 60
         };
         $('#objMonth').CanvasJSChart(optn);
         $('#chartMonthMobile').CanvasJSChart(optn);
         if(mgid === "" && !($('.ui.dimmer.hidden').length)) $('.ui.dimmer').transition('fade');
      }
   })
}

function groupMonthDataAjax(date) {
   let month = date.getMonth()+1;
   if(month < 10){
      month = "0"+month;
   }
   $.ajax({
      url: baseUrl+'/groupe/get-one.json',
      method: 'GET',
      data:{
         id: mgid
      },
      success: function (data) {
         let group = data;
         $('.tabular.menu .item').tab();
         let groupName = group.nom;
         let groupUsers = group['user'];
         let userData = [];
         for(let user of groupUsers) {
            $.ajax({
               url: baseUrl+'/userstat/get-data-monthly',
               data: {
                  year: date.getFullYear(),
                  month: month,
                  id: user.id
               },
               success: function (data) {
                  data = JSON.parse(data);
                  let count = 0;
                  for (let dataTemp of data.data) {
                     for (let tempData of dataTemp) {
                        count += tempData;
                     }
                  }
                  userData.push({y: count, label: user['firstName'], indexLabel:count.toString()});
                  if (userData.length === groupUsers.length) {
                     let optn = {
                        title:{
                           text:"Répartition des pas sur le mois"
                        },
                        axisY: {
                           title: "",
                           tickLength: 0,
                           lineThickness: 0,
                           margin: 0,
                           gridThickness: 0,
                           valueFormatString: " "
                        },
                        indexLabel:"#total",
                        indexLabelPlacement:"outside",
                        data: [
                           {
                              type:"column",
                              dataPoints: userData
                           }
                        ],
                        dataPointMaxWidth: 60
                     };
                     $('#groupChartMonthComputer').CanvasJSChart(optn);
                     $('#chartMonthGroupMobile').CanvasJSChart(optn);
                     if(!$('.ui.dimmer.hidden').length) $('.ui.dimmer').transition('fade');
                  }
               }
            });
            user['avatar'] = (user['avatar'] === null) ? getNoImageString() : user['avatar'];
            let groupImagesMonthlyComputer = $('#groupImagesMonthlyComputer');
            let groupImagesMonthlyMobile = $('#groupImagesMonthlyMobile');
            if ($('#groupImagesMonthlyComputer img').length === groupUsers.length) groupImagesMonthlyComputer.empty();
            if ($('#groupImagesMonthlyMobile img').length === groupUsers.length) groupImagesMonthlyMobile.empty();
            let toAppend = '<img src="' + user['avatar'] + '" alt="avatar utilisateur" class="ui image" height="auto" width="20%"/>';
            groupImagesMonthlyComputer.append(toAppend);
            groupImagesMonthlyMobile.append(toAppend);
            let padding = (44 - ((groupUsers.length - 1) * 5)) / groupUsers.length;
            padding += "%";
            groupImagesMonthlyMobile.css('margin-left', padding).css('margin-right', 100-padding)
            groupImagesMonthlyComputer.css('margin-left', padding).css('margin-right', 100-padding)
         }
      },
      error: console.log
   });
}
//endregion
//region utilities
function formatDate(d) {
   let month = months[d.getMonth()];
   let day = '' + d.getDate();
   let year = d.getFullYear();

   return "Week "+day+" "+month+" "+year;
}

function updateDay(date) {
   let monday = date;
   if(monday.getDay() !== 1){
      if(monday.getDay() === 0){
         monday.setDate(monday.getDate() - 6)
      }
      else{
         monday.setDate(monday.getDate() - monday.getDay()+1)
      }
   }
   let dateString = months[monday.getMonth()] + " " +monday.getDate();
   $('.weekCurr').text(dateString);
}

function updateDayMonth(date) {
   let dateString = months[date.getMonth()] + " " +date.getFullYear();
   $('.monthCurr').text(dateString);
}

function swapColors(){
   let chart = $('#objWeek').CanvasJSChart();
   if(chart){
      if(chart.data[1] !== undefined){
         colorSwap(chart.data[1]);
      }
      else{
         colorSwap(chart.data[0]);
      }
   }
}

function colorSwap(chartData){
   let color = chartData.legendMarkerColor;
   chartData.set("legendMarkerColor", (color === "#007677") ? "#00c800" : "#007677");
}

function getNoImageString(){
   return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAC4xJREFUeJztnVusFVcdh79zuBULEkpPI9JG24SaSq0BCyVphYeamBCTVgmBGuVQsDTaND5U4yWNPogxGo2JxlgIxsYHY2LiBWuNQRqKyoPWaorGAq0IbaQ3LlKp0Ar1YZ2j5+w9M2evmVm3Wb8v+T20nDN7zX/9v7P3zKyZDUIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghRFiGgJWhByFEjAwBXwVeB7YGHosQUTFRjvFIEiEolkOSCEG1HJJEZM0gckgSkS0rGUwOSSKyZSuSRIhKJIlIniHH298KbLf4+buBHZavMQwsA24GlgLXAlcCC4DZuN/H3HkNeBk4AxwFDgJ/Bh4FDmD++IkKXLyTDAHvAR4ETlhuX/GXF4CdwGr0h6qStiS5BLgHOGy5PSV8DgN3ATP7ZlUAzSUZwbyFh55opVmOAXcgCmkqyShwwXIbSpzZA1yD6KOpJJuQJF3JaWAtoo+mktzJZEnOA7uB+4E1wGJgLjow9MFM4DLgOuA24HPAXsycDDq/23wPOgWaSrIZeAzYAszzM2RhwXzMafs/Mtj87gSmBRlpxOhiYh6sAZ5gMElED5IkD6YD9wHn0MctayRJPiwFnqJ6fj8QbHQRI0nyYQT4PdVnt64ONrqIkST5MJdqSXaHG1pcjPT8tyTJhxGqP26tDze0OJgFHMGcsp2IJMmHpZQfuB8FZoQbWng+iinERSRJztxH+bxuCTiuoAwxeVWuJMmX6ZRfJzlEpisibqW/GJIkX95H+ZzeEnBcwfguxcV4rOBnJUkelC1Lsb3LNHmGgZew+8wpSbrP+DFpb54LOagQ3EhxIc5TvfBQknSb+ZSvAl4ScFze+TjFRRjk4pAk6Tb7KJ7He0IMZjjEi2LOfRfx6AC/uwOzjHpQtiNJUuKRkv+f1TvIfor/Sqyx2IbeSbrJ+ymevz0hB+WbYxQXYbHldiRJ93g7xXP3dMhB+eYsxUWYW2NbkqRbXE7xvL0YclC+uUhxEepeMZUk3WEW5Wc4s6GscZsgSbqDi/5IClcFkCTdQIKUpA0kSfpIkJK0hSRJGwlSkjaRJOkiQUrSNpIkTSRISVwgSdJDgpTEFZIkLSRISVwiSdJBgpTENZIkDSRISXwgSeJHgpTEF5IkbkL3R3BiKIAkiZcY+iMosRRAksRJLP0RjJgKIEniI6b+CEJsBZAkcRFbf3gnxgJIkniIsT+8EmsBuizJELAI80yyVcBNwFXE+dzbWPvDGzEXoEuSzAFGgV3AKYrHfxp4CPNM5DlhhtlHzP3hhdgLkLokszHfGX8Su/04NfZ7s/0PeRKx94dzUihAqpKsYOovyZwqh4Hlvgc+gRT6wympFCA1STYAr9JMjvGcH9teCFLpD2ekVIBUJNlA+eOU6uYiYSRJqT+ckFoBYpdkBe29cxS9k/j+uJVaf7ROigWIVZLZND/mmCqH8HvgnmJ/tEqqBYhRkvstx1Q3n/WwL+Ok2h+tkXIBYpJkDvancuvmJHCpw32ZSMr90QqpFyAWSUYtx9E0o472o5fU+6MxXShADJLsshxD0+xysA9FdKE/GtGVAoSUZAizTMSnIKfws3arK/1Rmy4VIJQkiyxft60samn8VXSpP2rRtQKEkKTsm4Jdx8c1ka71hzVdLIBvSd5t+XptZVXDcQ9CF/vDiq4WwKckegfpMF0ugC9JdAzSYbpeAB+SDFF+E5SrnERnsbyQQwF8SPJTy9domp/UGGMdcuiPSnIpgGtJNlpuv2k2Wo6vLrn0Ryk5FcClJJcCJyy3XzcngDdY7309cuqPQnIrgEtJPmO57br5VI39rktu/dFHjgVwJcklmPs1XMrx5Njr+CLH/phErgVwJcm7MHf+uZDjHLCs9h7XI9f++B85F8CVJOtwc0/6ugb7Wpec+wNQAVxK0tY7yTnCyEHFmLKhCwWYCewAvljz911+3Dpoue3ePIn/j1UT6UJ/NCL1AiwE9vP/cX+95nZcSTIL+DTwkuX2T2DOVvk8IC8i9f5oTMoFWAn8g/6xP0C9ZRiur5N8CPgx5feun8RcId+Iv+scU5Fyf7RCqgX4CNWf8b8HTKuxXV9rtxZiPoKtwqzKXYSe7h4lqRVgBvBtBmveH479vC0x3OMeC6n1R+ukVIA3Ab/Brnl/hjkOsEWSGFLqDyekUoCbgGexa9rx7KbeZ3pJkk5/OCOFAmzGXAuoI8d4fg3MrfHauUuSQn84JeYCzAC+RTMxJuZ3wPwa48hZkpj7wwuxFuAKYB/tyTGePwEjNcaTqySx9oc3YizAjcAz2DWkTf4KvLnGuHKT5Fri7A+vxFaAUeDfFeNqK08Bb6kxvhwkGQLuBV4hvv7wTiwFmA58o2I8LnIMWFxjrF2W5CrgV0y9T9kQQwFGgL0VY3GZ48CSGmPuoiSjDP6M4WwIXYBlwNGKcfjIi8DSGmPviiQjmDViNvuSDSEL8GH8HG8MklOYxY+2pC7J7cDz2NcrG0IUYDpmWXpoKXrzMrC6xv6kKMk84EHq1yobfBfgcuCRitcNnVeA99bYr5QkuRVzgqJJnbLBZwGWAn+veM1Ych64rcb+xS7JbMyZwjbul88GXwX4INXn1WPLa8CGGvsZqyQrMLfvtlWfbHBdgGnA1ypeJ+ZcAO6ssc8xSTID+ALwH8sxSZAxXBZgAWapeehGb5KLwMdq7HsMkiwBHrcchwTpwVUBbgD+VrH91PKJGjUIJckw8Ema3yIgQXBTgPXA2Yptp5rP16iFb0muwc0qaAlC8wJMA75Ssc0u5Ms16uJLkq2Yazk+6pANbRVgPvDLiu11Kd/E/gkkLiVZCDzsuQbZ0EYB3gE8XbGtLuY7mM/6NriQZD3+vpdEgmBfgHXAvyq20+V8H7Nsxoa2JLkM+EHAfc+GugUYBr5U8fu55EeYZwPb0FSSBdR/wosEsaROAeYDv6j43dzyMGYZhw1NJdlM+1+xIEEKsC3A9ZjbVUM3ZWx5BJhTWel+UpYkG2wKsBZ/pxFTzH7MMnIbUpUkGwYpwDDmuzdCvqWnkj9gjhFsSFGSbJiqAPOAn1f8nNKfA5jnCNuQmiTZUFWA62j+DUm55iBw5aCTMEZKkmRDWQFuB85U/LsydY5g1kbZkIok2VBWAB1vtJNngbdNMQe9T55PQZJsCN1AOeR5zHKcIjZhvnqt98as2CXJhtDNk0tOYJ45PM4VTH4WVdHdizFLkg2hGyen/BO4GXN890LBv6ckSTaEbprcUvXFo6+TjiTZELphlP6kIEk2hG4GpTixS5INoRtBKU/MkmTDq4RvBKU8FzCngicSWpILZESI2zUV+4bc1DNvISU5Q0YcIXwDKFMnJkmOkBF7CT/5ymCJRZJ9ZMR2wk+8MnhikGQnGXEv4SddsUtoSeo8qzhZbiD8hCv2CSnJO8mIIYrXBSnxJ4Qkx7F/qmTy7CT8ZCv1JRntmU+XkjxAhqwm/EQraUhyCxkyBBwm/EQrcUtygIy5i/CTrMQtySYyZibNvxpYCR9XkhzE/kHdneMOwk+wEqcktg/D6yx7CD/BSpySCMxznE4TfoKVdiTZOHl6JUkbrCX85CqSJGq2EX5yFUkSNbrC3p1IEgdMQ5J0KZLEEfq41Z1IEkesRWe3uhJJ4oirgd2En2BFkkTNeuAo4SdZaZbD9F8hlyQtMQPYAhwi/EQrdvkLZn3VjL5ZNUiSFhnC3COwA3iO8JOvFOc45manQe/niE6SrtzGeD3mBqwlmG9WeivwxrHMDDesLLgInMU8DPAZzLv748BvgScwjWzDVsxTbwblbswfSiGyIbp3EiFiQ5IIMQW2kiwPM0whwjGoJNvozjG1EFZMJYnkENlTJonkEGKMXkkkhxA9jEsiOYQoYTmSQwghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCFEYP4LGsFS1woTTe4AAAAASUVORK5CYII=";
}
//endregion