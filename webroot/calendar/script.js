const monthNames = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
    "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"
];

lineN = 1;
colN = 1;


function arraySearch(arr,val) {
    for (var i=0; i<arr.length; i++)
        if (arr[i] === val)
            return i;
    return false;
}

function initDropdown(){
    for(var i = new Date().getFullYear()-110; i <= new Date().getFullYear() ; i++){
        $('#years').append('<div class="item" data-value="' + i + '" id="year' + i + '">' + i + "</div>");
    }
    $('#year' + (new Date().getFullYear())).addClass('active').addClass('selected');
}


function isInt(n){
    return (n%1 === 0);
}

function isBisextile(n){
    if(isInt(n/4) && !isInt(n/100)){
        return true;
    }
    else{
        if(isInt(n/100) && isInt(n/400)){
            return true;
        }
    }
    return false;
}

function updateDate(){
    month = monthNames[today.getMonth()];
    $('#currMonth').text(month);
    $('#yearsdrop').dropdown('set text', today.getFullYear());
}

thonedays = [0,2,4,6,7,9,11];
otherDays = [3,5,8,10];


function getNbDays(){
    if(tempDate.getMonth() === 1){
        if(isBisextile(today.getFullYear())){
            return 29;
        }
        return 28;
    }
    else{
        if(thonedays.includes(tempDate.getMonth())){
            return 31;
        }
        else{
            return 30;
        }
    }
}

function updateCalendar(){
    $("#calendar").html(defaultTable);
    tempDate = new Date();
    console.log(today);
    tempDate.setFullYear(today.getFullYear());
    tempDate.setMonth(today.getMonth());
    tempDate.setDate(1);
    nbDays = getNbDays();
    currDay = tempDate.getDay();
    for(lineN = 1; lineN <= 6; lineN++){
        colN= 1;
        if(currDay === 0) currDay = 7;
        currLine = $('#line' + lineN);
        if(lineN === 1){
            while(colN < currDay){
                newLine = document.createElement('td');
                $(newLine).addClass('unselectable');
                $(newLine).attr('id', 'l'+lineN+'c'+colN);
                currLine.append(newLine);
                colN++
            }
        }

            while(colN <= 7){
                if(tempDate.getDate() <= getNbDays() && tempDate.getMonth() === today.getMonth()) {
                    newLine = document.createElement('td');
                    $(newLine).attr('id', 'l' + lineN + 'c' + colN);
                    $(newLine).text(tempDate.getDate());
                    currLine.append(newLine);
                    tempDate.setDate(tempDate.getDate() + 1);
                    currDay = tempDate.getDay();
                }
                else{
                    newLine = document.createElement('td');
                    $(newLine).addClass('unselectable');
                    $(newLine).attr('id', 'l'+lineN+'c'+colN);
                    currLine.append(newLine);
                }
                colN++;
            }
    }


}

$(document).ready(function () {
    initDropdown();
    $('#years').dropdown();
    today = new Date();
    $('#dropplace').text(today.getFullYear());
    today.toLocaleDateString('fr-FR', {  weekday: 'long' });
    defaultTable = $("#calendar").html().toString();
    updateCalendar();
    updateDate();

    $('body').on('click', '#prev', function (event) {
        event.preventDefault;
       today.setMonth(today.getMonth()-1);
        updateCalendar();
       updateDate();
    });

    $('.popup').on('click', '#next', function (event) {
        event.preventDefault;
        if(today.getMonth()+1 <= new Date().getMonth()){
            today.setMonth(today.getMonth()+1);
            updateCalendar();
            updateDate();
        }
        else{
            if(today.getFullYear() < new Date().getFullYear()){
                today.setMonth(today.getMonth()+1);
                updateCalendar();
                updateDate();
            }


        }

    });

    $('table').on('click', 'td', function () {
        var clicked = $(this);
        if(!clicked.hasClass('unselectable') && !clicked.hasClass('day')){
            var temp = $(this).attr('id');
            temp = temp.split('');
            var nbGris = $('#line1 .unselectable').length;
            currMonth = $("#currMonth");
            var day = (temp[1]-1)*7 + (temp[3]-nbGris);
            if(!(day > today.getDate() && today.getMonth() >= new Date().getMonth() && today.getFullYear() >= new Date().getFullYear())){
                $("#displayDate").html('Vous avez sélectionné le '+day+" "+currMonth.html() + " " + today.getFullYear());
                var monthName = currMonth.html().split(' ')[0];
                var monthNo = arraySearch(monthNames, monthName)+1;
                if(monthNo < 9) monthNo = "0"+monthNo;
                if(day < 9) day = "0"+day;
                var currYear = today.getFullYear();
                var input = $("#inputDate");
                var value = currYear+"-"+monthNo+"-"+day+" 00:00:00";
                input.attr('value', value);
            }
        }
    });

    $('#years').on('click', '.item', function () {
        var tempyear = parseInt($('#yearsdrop').dropdown('get value'));
        console.log(tempyear);
        if((tempyear < new Date().getFullYear())){
            today.setFullYear(tempyear);
            updateCalendar();
        }
        else{
            if(today.getMonth() <= new Date().getMonth()){
                today.setFullYear(tempyear);
                updateCalendar();
            }
        }
        updateDate();
    });
});

/* Auteur : Théo Baumgartner */