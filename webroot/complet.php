<?php
session_name('CAKEPHP');
session_start();
function redirect($url){
    if($url != null){
        header('Location: '.$url);
        exit();
    }
    else{
        http_response_code(500);
    }
}
    if(empty($_POST)){
        if(!empty($_GET)){
            $_POST['case'] = $_GET['case'];
            foreach($_GET as $key=>$val){
                if($key != 'case'){
                    $_POST['json'][$key] = $val;
                }
            }
            $_POST['json'] = json_encode($_POST['json']);
        }
        else{
            http_response_code(500);
            die();
        }
    }

    /*
    On lance un script qui vas s'exécuter à chaque requêtes à l'API
    */
    // On met le json dans une variable $varReq
    $varReq = json_decode($_POST['json'], true);
    foreach($varReq as $key=>$val){
        $_SESSION[$key] = $val;
    }
    $url = null;
    // On fait un switch pour savoir quelle fonction lancer
    switch ($_POST['case']) {
        //------------------------------------------ USER ----------------------------------------------

        // Ajout d'un utilisateur
        case "add_user":
            $url = '/user/register-check';
            break;
        // Récupération d'un utilisateur
        case "get_user":
            $url = '/user/get-one';
            break;
        // Recupération de tous les utilisateurs
        case "get_all_user":
            $url='/user/get-all-users.json';
            break;
        // Suppression d'un utilisateur
        case "delete_user":
            $url='/user/delete-one';
            break;
        // modification d'un utilisateur
        case "update_user":
            $url = '/user/update-one';
            break;
        // verifie que l'email n'existe pas
        case "check_email":
            $url = '/user/register-check';
            break;
            // vérifie la connection
        case "check_pwd":
            $url = '/user/connect';
            break;
            
        //------------------------------------------ STAT ----------------------------------------------

        // Ajoue de stat
        case "add_stat":
            $stat = new Stat($pdo);
            $stat->add_stat($varReq); 
            break;
        // Obtention des stat d'un utilisateur
        case "get_stat":
            $stat = new Stat($pdo);
            $stat->get_stat($varReq); 
            break;
        // Obtention de toute les stat
        case "get_all_stat":
            $stat = new Stat($pdo);
            $stat->get_all_stat($varReq); 
            break;
        // modification d'une stat
        case "update_stat":
            $stat = new Stat($pdo);
            $stat->update_stat($varReq); 
            break;
        // supression de stat
        case "delete_stat":
            $stat = new Stat($pdo);
            $stat->delete_stat($varReq); 
            break;
        // renvoie le nombre de pas par interval
        case "get_step_interval":
            $stat = new Stat($pdo);
            $stat->sommeStep($varReq); 
            break;

        //------------------------------------------ OBJECTIF ----------------------------------------------

        // Ajoue d'objectif
        case "add_objective":
            $objectif = new Objectif($pdo);
            $objectif->add_objective($varReq); 
            break;
        // Obtention des objectif d'un utilisateur
        case "get_objective":
            $objectif = new Objectif($pdo);
            $objectif->get_objective($varReq); 
            break;
        // Obtention de l'objectif actuelle
        case "get_actual_objective":
            $objectif = new Objectif($pdo);
            $objectif->get_actual_objective($varReq); 
            break;
        // modification d'un objectif
        case "update_objective":
            $objectif = new Objectif($pdo);
            $objectif->update_objective($varReq); 
            break;
        // supression d'un objectif
        case "delete_objective":
            $objectif = new Objectif($pdo);
            $objectif->delete_objective($varReq); 
            break;

        //------------------------------------------ GROUPE ----------------------------------------------

        // Ajoue d'un groupe
        case "add_group":
            $groupe = new Groupe($pdo);
            $groupe->add_group($varReq); 
            break;
        // Obtention des droit d'un groupe
        case "get_group":
            $groupe = new Groupe($pdo);
            $groupe->get_group($varReq); 
            break;
        // modification d'un groupe
        case "update_group":
            $groupe = new Groupe($pdo);
            $groupe->update_group($varReq); 
            break;
        // supression d'un objectif
        case "delete_group":
            $groupe = new Groupe($pdo);
            $groupe->delete_group($varReq); 
            break;
        case "indexG":
            $groupe = new Groupe($pdo);
            echo $groupe->index();
            break;
        case "indexG2":
            $groupe = new Groupe($pdo);
            echo $groupe->indexG2($varReq);
            break;

        //------------------------------------------ USER_GROUPE ----------------------------------------------

        // Ajoue d'un utilisateur à un groupe
        case "add_user_group":
            $user_groupe = new User_Groupe($pdo);
            $user_groupe->add_user_group($varReq); 
            break;
        // Obtention de tout les utilisateur d'un groupe
        case "get_groupe_all_user":
            $user_groupe = new User_Groupe($pdo);
            $user_groupe->get_groupe_all_user($varReq); 
            break;
        // Obtention de tout les groupe d'un utilisateur
        case "get_user_all_groupe":
            $user_groupe = new User_Groupe($pdo);
            $user_groupe->get_user_all_groupe($varReq); 
            break;
        // supression d'un utilisateur à un groupe
        case "delete_user_groupe":
            $user_groupe = new User_Groupe($pdo);
            $user_groupe->delete_user_groupe($varReq); 
            break;

        //------------------------------------------ SCORE ----------------------------------------------
        
        // création d'un score (permet d'en ajouter plusieurs d'un coup)
        case "create_score":
            $score = new Score($pdo);
            $score->create_score($varReq); 
            break;
        // Ajout d'un utilisateur à un groupe
        case "add_score":
            $score = new Score($pdo);
            $score->add_score($varReq); 
            break;
        // Obtention de tout les utilisateurs d'un groupe
        case "get_score":
            $score = new Score($pdo);
            $score->get_score($varReq); 
            break;
        // Obtention des score des utilisateurs pour 1 objectif
        case "get_all_score_objectif":
            $score = new Score($pdo);
            $score->get_all_score_objectif($varReq);
            break;
        // Obtention de tout les groupe d'un utilisateur
        case "update_score":
            $score = new Score($pdo);
            $score->update_score($varReq);
            break;
        // supression d'un utilisateur à un groupe
        case "delete_score":
            $score = new Score($pdo);
            $score->delete_score($varReq); 
            break;

        default:
            echo '{"response" : "Case inexistant :: '.var_dump($_POST["case"]).' "}';
            http_response_code(500);
            break;
    }

    redirect($url);

    ?>
<input type="hidden" name="m_email" value="jose@test.fr"/>
