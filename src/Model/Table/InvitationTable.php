<?php


namespace App\Model\Table;


use Cake\ORM\Table;

class InvitationTable extends  Table
{

    public function initialize(array $config)
    {
        parent::initialize($config); // TODO: Change the autogenerated stub
        $this->hasOne('User');
        $this->hasOne('Groupe');
    }

}