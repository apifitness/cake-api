<?php


namespace App\Controller;


use Cake\Event\Event;

class GroupeController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $action = $this->getRequest()->getParam('action');
        if(in_array($action, ['index'])){
            if($this->getRequest()->getSession()->read('id') === null){
                return $this->redirect('/');
            }
        }
    }

    /** WEB */
    public function index(){
        if($this->getRequest()->getSession()->read('id') == null) $this->redirect('/');
    }

    /** API */
    public function getOne(){
        $id = $this->getRequest()->getQuery('id');
        $groupReturn = $this->Groupe->find()->select()->where(['id'=>$id])->contain(['user'])->first();
        if($groupReturn){
            $this->set($groupReturn->toArray());
            $this->set('_serialize', array_keys($groupReturn->toArray()));
        }
        else{
            die();
        }
    }

    public function getAll(){
            $userId  = intval($this->getRequest()->getQuery('id'));
            $userGroupes = $this->Groupe->User->find()->select()->where(['id'=>$userId])->contain(['Groupe'=>['User']])->first();
            $userGroupes = $userGroupes['groupe'];
            foreach ($userGroupes as $gkey=>$grp){
                foreach ($grp['user'] as $ukey=>$usr){
                    unset($usr['email']);
                    unset($usr['password']);
                }
            }
            $this->set($userGroupes);
            $this->set('_serialize', array_keys($userGroupes));
    }

    public function getWhereOwner(){
        $userId  = intval($this->getRequest()->getQuery('id'));
        $userGroupes = $this->Groupe->find()->select()->where(['owner'=>$userId])->toArray();
        $this->set($userGroupes);
        $this->set('_serialize', array_keys($userGroupes));
    }

    public function getOneByYear(){
        $keys = array('Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre');
        $year = $this->getRequest()->getQuery('year');
        $id = $this->getRequest()->getQuery('id');
        $results = array();
        for($i = 0; $i < 12; $i++){
            $currKey = $keys[$i];
            $month = $i < 10 ? "0".$i:$i;
            $date = $year."-".$month."-%";
            $results[$currKey] = $this->Groupe->find()
                ->select()
                ->where(['groupe.id'=>$id])
                ->contain([
                    'user'=>[
                        'userstat',
                        function($q) use($date){
                            return $q->select()->where(['date LIKE'=>$date]);
                        }
                    ]
                ])
                ->toArray();
        }
        $this->set($results);
        $this->set('_serialize', array_keys($results));
    }

    public function createNew(){
        $groupName = $this->getRequest()->getQuery('nom');
        $userId = $this->getRequest()->getQuery('id');
        $data = array();
        $data['nom'] = $groupName;
        $data['owner'] = $userId;
        if(!$this->Groupe->find()->select(['nom'])->where(['nom'=>$groupName])->first() === null || trim($groupName) == ""){
            $this->set(['status'=>'ERR']);
            $this->set('_serialize', ['status']);
        }
        else{
            $currGrp = $this->Groupe->save($this->Groupe->newEntity($data));
            $toLink = $this->Groupe->User->find()->select()->where(['id'=>$userId])->contain(['Groupe'])->first();
            if(!count($toLink['groupe'])){
                $toLink['mainGroupId'] = $currGrp['id'];
                $toLink = $this->Groupe->User->save($toLink);
            }
            $this->Groupe->User->link($currGrp, [$toLink]);

            $this->set(['status'=>$groupName]);
            $this->set('_serialize', ['status']);
        }
    }

    public function leave(){
        $groupName = $this->getRequest()->getQuery('nom');
        $userId = $this->getRequest()->getQuery('id');
        $toDelete = $this->Groupe->find()->select()->where(['nom'=>$groupName])->toArray();
        $toUnlink = $this->Groupe->User->get($userId);
        foreach($toDelete as $td){
            $this->Groupe->User->unlink($td, [$toUnlink]);
            if($td['owner'] == $userId){
                $this->Groupe->delete($td);
            }
        }
        die();
    }

    public function join(){
        $user_id = $this->getRequest()->getQuery('user_id');
        $group_id = $this->getRequest()->getQuery('group_id');
        $inv_id = $this->getRequest()->getQuery('inv_id');
        $group = $this->Groupe->get($group_id);
        $user = $this->Groupe->User->get($user_id);
        $inviteDelete = $this->Groupe->Invitation->get($inv_id);
        $this->Groupe->User->link($group, [$user]);
        $this->Groupe->Invitation->delete($inviteDelete);
        $this->set(['status'=>'ok']);
        $this->set('_serialize', ['status']);
    }
}