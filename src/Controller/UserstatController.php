<?php


namespace App\Controller;


use DateTime;

class UserstatController extends AppController
{
    /** API FUNCTIONS ONLY */

    public function getAllDataUser(){
        $id = $this->getRequest()->getQuery('id');
        $data = $this->Userstat->find()->select()->where(['user_id'=>$id])->toArray();
        $this->autoRender = false;
        echo json_encode($data);
        die();
    }

    public function getYearDataUser(){
        $year = $this->getRequest()->getQuery('year');
        $data = array();
        for($i = 1; $i <= 12; $i++){
            $date = $year."-";
            if($i < 10){
                $date.="0".$i."%";
            }
            else{
                $date.=$i."%";
            }
            $id = $this->getRequest()->getQuery('id');
            $dataMonth = $this->Userstat->find()->select()->where(['user_id'=>$id, 'date LIKE'=>$date])->toArray();
            $data[] = $dataMonth;
        }
        $this->autoRender = false;
        echo json_encode($data);
        die();
    }

    function getDataMonth(){
        $year = $this->getRequest()->getQuery('year');
        $month =  $this->getRequest()->getQuery('month');
        $date = $year."-".$month."%";
        $id = $this->getRequest()->getQuery('id');
        $data = $this->Userstat->find()->select()->where(['user_id'=>$id, 'date LIKE'=>$date])->toArray();
        $this->autoRender = false;
        $layout = 'ajax';
        echo json_encode($data);
        die();
    }

    function getDataMonthly(){
        // region Query Parameters (GET)
        $year = $this->getRequest()->getQuery('year');
        $month =  $this->getRequest()->getQuery('month');
        $id = $this->getRequest()->getQuery('id');
        //endregion
        //region Month limits
        $firstDay = new DateTime(); // Create a datetime (will come in handy when we'll have to edit it
        $lastDay = new DateTime(); // ^
        $firstDay->setDate($year, $month, '01'); // Set the date to the first day of the selected month
        $lastDay->setDate($year, $month, $firstDay->format('t')); // Set the date to the last day of the selected month
        //endregion
        $dayOfWeekFirst = $firstDay->format('N'); // Get the day of the week for the first day of the month
        $dayOfWeekLast = $lastDay->format('N');
        // region First Day of Month treatment
        if($dayOfWeekFirst != '1') { // If it's not a monday
            if (intval($dayOfWeekFirst) <= 4) {
                $firstDay->modify('-' . $dayOfWeekFirst . ' days');
                $firstDay->modify('+1 days');
            } else {
                $nbDays = (7 - intval($dayOfWeekFirst)) + 1;
                $firstDay->modify('+' . $nbDays . ' days');
            }
        }
        //endregion
        //region Last Day of Month treatment
        if($dayOfWeekLast != '7') { // If it's not a sunday
            if (intval($dayOfWeekLast) < 4) {
                $lastDay->modify('-' . $dayOfWeekLast . ' days');
            } else {
                $nbDays = (7 - intval($dayOfWeekLast));
                $lastDay->modify('+' . $nbDays . ' days');
            }
        }
        //endregion
        $beginDate = $firstDay->format('Y-m-d');
        $resultsWeek = array();
        $resultsDay = array();
        //region Data Treatment Loops
        /* Récupération des données */
        $resultsTemp = $this->Userstat->find()
            ->select(['count', 'date'])
            ->where(
                [
                    'date >='=>$firstDay->format('Y-m-d').' 00:00:00',
                    'date <='=>$lastDay->format('Y-m-d').' 23:59:59',
                    'user_id'=>$id
                ]
            )
            ->order(['date'=>'ASC'])
            ->toArray();
        /* Classement par date */
        foreach ($resultsTemp as $res){
            $resultsDay[$res->date->format('Y-m-d')] = (isset($resultsDay[$res->date->format('Y-m-d')])) ? $resultsDay[$res->date->format('Y-m-d')] + $res['count'] : $res['count'];
            $firstDay->setDate($res->date->format('Y'),$res->date->format('m'),$res->date->format('d'));
        }
        $firstDay->modify('+1 days'); // Sert à déterminer que les prochaines données sont à la suite
        /* Ajout de "0" là où il n'y a pas de données */
        while($firstDay <= $lastDay){
            $resultsDay[$firstDay->format('Y-m-d')] = 0;
            $firstDay->modify('+1 days');
        }
        $resultsWeek = array();
        $i = 0; // Curseur des jours
        $j = 0; // Curseur des semaines
        /* Tri par semaines */
        foreach ($resultsDay as $res){
            $resultsWeek[$j][] = $res; // Pour la semaine j, ajout des résultats sur le jour i
            $i++;
            if($i == 7){
                $i = 0;
                $j++;
            }
        }
        //endregion
        echo json_encode(['data'=>$resultsWeek, 'beginDate'=>$beginDate]);
        die();
    }

    function getWeekData(){
        $id = $this->getRequest()->getQuery('id'); // Récupération de l'ID utilisateur
        $date = new DateTime(); // On récupère la date actuelle
        $data = array(); // Tableau où les données seront placées
        $query = $this->getRequest()->getQuery(); // Récupération des paramètres
        $date->setDate(intval($query['year']), intval($query['month']), intval($query['currDay'])); // Modification de la date pour arriver à la date du
        $date->modify('-'.$date->format('w').' days');
        $date->modify('+1 days');
        for($i = 0; $i < 7; $i++){
            $data[] = $this->Userstat->find()->select(['count'])->where(['user_id'=>$id, "date LIKE"=>$date->format('Y-m-d')."%"])->toArray();
            if(empty($data[$i])){
                $data[$i] = array(['count'=>0]);
            }
            $date->modify('+1 days');
        }
        $this->autoRender = false;
        echo json_encode($data);
        die();
    }

    function getCurrWeekData(){
        $id = $this->getRequest()->getQuery('id'); // Récupération de l'ID utilisateur
        $date = new DateTime(); // On récupère la date actuelle
        $data = array(); // Tableau où les données seront placées
        $date->modify('-'.$date->format('w').' days');
        $date->modify('+1 days');
        $count = 0;
        for($i = 0; $i < 7; $i++){
            $data[] = $this->Userstat->find()->select(['count'])->where(['user_id'=>$id, "date LIKE"=>$date->format('Y-m-d')."%"])->toArray();
            foreach($data[$i] as $dat){
                $count+= $dat['count'];
            }
            $date->modify('+1 days');
        }
        $this->autoRender = false;
        echo $count;
        die();
    }

}