<?php


namespace App\Controller;


use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class InvitationController extends  AppController
{

    public function beforeFilter(Event $event)
    {
        $action = $this->getRequest()->getParam('action');
        if(in_array($action, ['creation', 'index'])){
            if($this->getRequest()->getSession()->read('id') === null){
                return $this->redirect('/');
            }
        }
        return parent::beforeFilter($event);
    }

    /** API FUNCTIONS */

    public function invite(){
        $mail = $this->getRequest()->getQuery('mailU');
        $groupName = $this->getRequest()->getQuery('groupName');
        $usrId = $this->getRequest()->getQuery('id');
        $currgrp = $this->Invitation->Groupe->find()->select(['id'])->where(['nom'=>$groupName, 'owner'=>$usrId])->contain(['User'])->first();
        foreach($currgrp['user'] as $usrTmp){
            unset($usrTmp['_joinData']);
        }
        $currUsr = $this->Invitation->User->find()->select()->where(['email'=>$mail])->first();
        $sender = $this->Invitation->User->get($usrId);
        $mailSubject = $sender['firstName']." vous a invité à un groupe !";
        $mailContent = "Pour accepter son invitation, veuillez vous rendre sur http://break-first.eu/API_F_T/Front et créer un compte si ce n'est pas le cas puis vous rendre dans la catégorie \"Invitations\"";

        unset($sender['_joinData']);
        $data['user_mail'] = $mail;
        $data['groupe_id'] = $currgrp['id'];
        $data['sender'] = $usrId;
        if($sender == null){
            header('HTTP/1.1 501 Vous n\'existez pas');
            die();

        }
        if($currgrp == null){
            header('HTTP/1.1 501 Le Groupe n\'existe pas ou vous n\'en êtes pas le propriétaire.');
            die();

        }
        if(gettype(intval($usrId)) !== gettype(542)){
            header('HTTP/1.1 501 Identifiant invalide');
            die();

        }
        if($this->Invitation->find()->select()->where($data)->first() !== null){
            header('HTTP/1.1 501 Invitation déjà existante');
            die();

        }
        if(in_array($currUsr, $currgrp['user'])){
            header('HTTP/1.1 501 Utilisateur déjà dans le groupe');
            die();

        }
        $this->Invitation->save($this->Invitation->newEntity($data));
        Email::deliver($mail, $mailSubject, $mailContent, ['from'=>'webmaster@break-first.eu']);
        die();
    }

    public function countById(){
        $id = $this->getRequest()->getQuery('id');
        $toReturn = array();
        $users = TableRegistry::getTableLocator()->get('User');
        $currUsr = $users->get($id);
        $toReturn['recv'] = $this->Invitation->find()->select()->where(['user_mail'=>$currUsr['email']])->toArray();
        echo count($toReturn['recv']);
        die();
    }

    public function getAllById(){
        $id = $this->getRequest()->getQuery('id');
        $toReturn = array();
        $users = TableRegistry::getTableLocator()->get('User');
        $currUsr = $users->get($id);
        $toReturn['recv'] = $this->Invitation->find()->select()->where(['user_mail'=>$currUsr['email']])->toArray();
        foreach($toReturn['recv'] as $key=>$received){
            $sender = $users->get($received['sender']);
            $toReturn['recv'][$key]['username'] = $sender['firstName']." ".$sender['lastName'];

            $currGroupe = $this->Invitation->Groupe->get($received['groupe_id']);
            $toReturn['recv'][$key]['groupname'] = $currGroupe['nom'];
        }
        $toReturn['sent'] = $this->Invitation->find()->select()->where(['sender'=>$id])->toArray();
        foreach($toReturn['sent'] as $key=>$sent){
            $receiver = $users->find()->select()->where(['email'=>$sent['user_mail']])->first();
            $toReturn['sent'][$key]['username'] = $receiver['firstName']." ".$receiver['lastName'];

            $currGroupe = $this->Invitation->Groupe->get($sent['groupe_id']);
            $toReturn['sent'][$key]['groupname'] = $currGroupe['nom'];
        }
        $this->set($toReturn);
        $this->set('_serialize', array_keys($toReturn));
    }

    public function createInvite(){
        $data = array();
        $user_id = $this->getRequest()->getQuery('user_id');
        $group_name = $this->getRequest()->getQuery('group_name');
        $message = $this->getRequest()->getQuery('message');
        $data['user_id'] = $user_id;
        $data['group_name'] = $group_name;
        $data['message'] = $message;
        if($this->Invitation->save($this->Invitation->newEntity($data))){
           $this->set(['status'=>'ok']);
           $this->set('_serialize', ['status']);
        }
        else{
            $this->set(['status'=>'err']);
            $this->set('_serialize', ['status']);
        }
    }

    public function refuse(){
        $iid = $this->getRequest()->getQuery('inv_id');
        $this->Invitation->delete($this->Invitation->get($iid));
        $this->set(['status'=>'ok']);
        $this->set('_serialize', ['status']);
    }

    /** WEB PAGE FUNCTIONS */
    public function index(){
    }



    public function creation(){
    }

}