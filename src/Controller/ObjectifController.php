<?php


namespace App\Controller;


use Cake\Event\Event;
use DateTime;

class ObjectifController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $action = $this->getRequest()->getParam('action');
        if(in_array($action, ['nouveau', 'sauvegarderNouveau'])){
            if($this->getRequest()->getSession()->read('id') === null){
                return $this->redirect('/');
            }
        }
    }

    /** API FUNCTIONS */
    public function getCurrentObjectiveUser(){
        $id = $this->getRequest()->getQuery('id');
        $date = date('Y-m-d H-i-s');
        $returnObject = $this->Objectif->User->find()->select()->where(['User.id'=>$id])->contain('Objectif', function ($q) use($date){
            return $q
                ->select()
                ->where(['DateDebut <='=>$date, 'DateFin >='=>$date]);
        })->first();
        if($returnObject !== null && !empty($returnObject['objectif'])){
            $returnObject =  $returnObject['objectif'];
            $this->set($returnObject);
            $this->set('_serialize', array_keys($returnObject));
        }
        else{
            $this->set(['code'=>'-1']);
            $this->set('_serialize', ['code']);
        }
    }

    public function getCurrentObjectiveGroup(){
        $id = $this->getRequest()->getQuery('id');
        $date = date('Y-m-d H-i-s');
        $returnObject = $this->Objectif->Groupe->find()->select()->where(['Groupe.id'=>$id])->contain('Objectif', function ($q) use($date){
            return $q
                ->select(['objectif'])
                ->where(['DateDebut <='=>$date, 'DateFin >='=>$date]);
        })->first();
        if($returnObject !== null){
            $returnObject =  $returnObject['objectif'];
            $this->set($returnObject);
            $this->set('_serialize', array_keys($returnObject));
        }
        else{
            $this->set(['code'=>'-1']);
            $this->set('_serialize', ['code']);
        }
    }

    public function save(){
        $this->getRequest()->getSession()->delete('error');
        $data = $this->getRequest()->getData();
        $dayofweek = date('w', strtotime($data['DateDebut']));
        try {
            $dateTemp = new DateTime($data['DateDebut']);
        } catch (\Exception $e) {
        }
        $dateTemp->modify('+6 days');
        $dateTemp->modify('+23 hours');
        $dateTemp->modify('+59 minutes');
        $dateTemp->modify('+59 seconds');
        $data['DateFin'] = $dateTemp->format('Y-m-d H:i:s');
        if(!empty($this->Objectif->User->find()->select()->where(['User.id'=>$this->getRequest()->getQuery('id')])->contain('Objectif', function ($q) use($data){
            return $q
                ->select()
                ->where(['DateDebut <='=>$data['DateFin'], 'DateFin >='=>$data['DateFin']]);
        })->first()['objectif'])){
            $this->redirect($this->referer());
            $this->getRequest()->getSession()->write('error', 'Vous avez déjà un objectif pour la semaine sélectionnée.');
        }
        else{
            if($dayofweek != 1){
                $this->redirect($this->referer());
                $this->getRequest()->getSession()->write('error', 'Le jour sélectionné n\' est pas un Lundi.');
                $this->set(compact($data));
            }
            else{
                $save = true;
                foreach($data as $datTemp){
                    if(empty(trim($datTemp))) $save = false;
                }
                if($save){
                    $data['step'] = 0;
                    $data['etat'] = 0;
                    $toSave = $this->Objectif->newEntity($data);
                    $objId = $this->Objectif->save($this->Objectif->newEntity($data));
                    if(!$objId){
                        $this->set(['code'=>'-1']);
                        $this->set('_serialize', ['code']);
                    }
                    else{
                        $toLink = $this->Objectif->User->get($this->getRequest()->getQuery('id'));
                        $this->Objectif->User->link($objId, [$toLink]);
                        $this->set(['code'=>'1']);
                        $this->set('_serialize', ['code']);
                    }
                }
                else{
                    $this->set(['code'=>'-1']);
                    $this->set('_serialize', ['code']);
                }
            }
        }
    }

    /** WEB PAGE FUNCTIONS */

    public function nouveau(){
    }

    public function sauvegarderNouveau(){
        $this->getRequest()->getSession()->delete('error');
        $data = $this->getRequest()->getData();
        $dayofweek = date('w', strtotime($data['DateDebut']));
        try {
            $dateTemp = new DateTime($data['DateDebut']);
        } catch (\Exception $e) {
        }
        $dateTemp->modify('+6 days');
        $dateTemp->modify('+23 hours');
        $dateTemp->modify('+59 minutes');
        $dateTemp->modify('+59 seconds');
        $data['DateFin'] = $dateTemp->format('Y-m-d H:i:s');
        if(!empty($this->Objectif->User->find()->select()->where(['User.id'=>$this->getRequest()->getQuery('id')])->contain('Objectif', function ($q) use($data){
            return $q
                ->select()
                ->where(['DateDebut <='=>$data['DateFin'], 'DateFin >='=>$data['DateFin']]);
        })->first()['objectif'])){
            $this->redirect($this->referer());
            $this->getRequest()->getSession()->write('error', 'Vous avez déjà un objectif pour la semaine sélectionnée.');
        }
        else{
            if($dayofweek != 1){
                $this->redirect($this->referer());
                $this->getRequest()->getSession()->write('error', 'Le jour sélectionné n\' est pas un Lundi.');
                $this->set(compact($data));
            }
            else{
                $save = true;
                foreach($data as $datTemp){
                    if(empty(trim($datTemp))) $save = false;
                }
                if($save){
                    $data['step'] = 0;
                    $data['etat'] = 0;
                    $toSave = $this->Objectif->newEntity($data);
                    $objId = $this->Objectif->save($this->Objectif->newEntity($data));
                    if(!$objId) $this->redirect($this->referer());
                    else{
                        $toLink = $this->Objectif->User->get($this->getRequest()->getSession()->read('id'));
                        $this->Objectif->User->link($objId, [$toLink]);
                        $this->redirect('/displaydata?id='.$this->getRequest()->getSession()->read('id'));
                    }
                }
                else{
                    $this->redirect($this->referer());
                    $this->getRequest()->getSession()->write('error', 'L\'un des champs requis a été laissé vide.');
                    $data = $this->Objectif->newEntity($data);
                    $this->set(compact($data));
                }
            }
        }

    }

}