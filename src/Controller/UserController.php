<?php


namespace App\Controller;

use Cake\Event\Event;
use Cake\Http\Cookie\CookieCollection;
use Cake\I18n\Date;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Http\Cookie\Cookie;
use DateTime;

class UserController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $action = $this->getRequest()->getParam('action');
        if(in_array($action, ['getDataObjectif', 'displayData'])){
            if(!$this->getRequest()->getSession()->check('id')){
                return $this->redirect('/');
            }
        }
        return parent::beforeFilter($event);
    }

    function index(){
        $cookie = $this->getRequest()->getCookie('id');
        if($cookie != null){
            return $this->redirect('/displaydata');
        }
    }

    /** API FUNCTIONS */
    // Check the data sent when someone creates a new account
    function registerCheck(){
        if($this->getRequest()->getSession()->check('m_email')){
            $checkExistant = $this->User->find()->select()->where(['email'=>$this->getRequest()->getSession()->consume('m_email')])->first();
            if($checkExistant == null){
                http_response_code(200);
            }
            else{
                http_response_code(500);
            }
        }
        else{
            $data = $this->getRequest()->getData();
            $path = $data['avatar']['tmp_name'];
            $type = $data['avatar']['type'];
            if($path != ""){
                $file = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
                $data['avatar'] = $base64;
            }
            else{
                $data['avatar'] = "";
            }
            $data['password'] = hash('sha256', $data['password']);
            $newUser = $this->User->newEntity($data);
            $checkExistant = $this->User->find()->select()->where(['email'=>$newUser['email']])->first();
            if(gettype($newUser) != "boolean" && $checkExistant == null){
                if($this->User->save($newUser)){
                    echo '{"goodmess":"User Saved successfully."}';
                    $mailContent = "Bienvenue, ".$data['firstName']."  !
Nous sommes très heureux de vous compter parmi nous et espérons que vous passerez un bon moment en utilisant l'application :)";
                    $mail = Email::deliver($data['email'], 'Inscription réussie !', $mailContent, ['from' => 'webmaster@break-first.eu'], false);
                    $mail->send();
                    $sess = $this->getRequest()->getSession();
                    $sess->write('id', $newUser['id']);
                    $sess->write('avatar', $newUser['avatar']);
                }
                else{
                    echo '{"badmess":"Problem while saving user"}';
                }
            }
            else{
                echo '{"badmess":"Email déjà utilisée."}';
            }
        }
        die();
    }

    function deleteOne(){
        $sess = $this->getRequest()->getSession();
        if($sess->check('m_id')){
            $this->User->delete($this->User->get($sess->consume('m_id')));
            http_response_code(200);
            die();
        }
        else{
            http_response_code(403);
        }
    }

    // Get one user by email and pass
    function getOne(){
        $sess = $this->getRequest()->getSession();
        if($sess->check('m_id')){
            $usr = $this->User->find()->select()->where(['id'=>$sess->consume('m_id')])->contain(['Groupe'])->first();
            $idGroup = $usr->groupe[0]->id;
            unset($usr['groupe']);
            unset($usr['email']);
            unset($usr['password']);
            unset($usr['avatar']);
            $usr = json_encode($usr);
            echo '{ "m_idGroup" : '.$idGroup.', users: '.$usr.'}';
            http_response_code(200);
            die();
        }
        elseif($sess->check('m_email') && $sess->check('m_password')){
            $usr = $this->User->find()->select()->where(['email'=>$sess->consume('m_email'), 'password'=>$sess->consume('m_password')])->contain(['Groupe'])->first();
            $idGroup = $usr->groupe[0]->id;
            unset($usr['groupe']);
            unset($usr['email']);
            unset($usr['password']);
            unset($usr['avatar']);
            $usr = json_encode($usr);
            echo '{ "m_idGroup" : '.$idGroup.', users: '.$usr.'}';
            http_response_code(200);
            die();
        }
        else{
            http_response_code(403);
            die();
        }
    }

    // Get all the users in DB
    function getAllUsers(){
         $jsonVar = $this->User->find()
             ->selectAllExcept($this->User,['password', 'email', 'sizeValue', 'weightValue', 'id']) // To prevent people from hacking accounts
             ->toArray();
         foreach($jsonVar as $key=>$user){
             unset($user['avatar']);
         }
         if(!json_encode($jsonVar)){
             $problem = array();
             $problem['errorMsg'] = json_last_error_msg();
             $this->set($problem);
             $this->set('_serialize', array_keys($problem));
         }
         else{
             $this->set($jsonVar);
             $this->set('_serialize', array_keys($jsonVar));
         }
    }

    function updateOne(){
        $tabValChange = array();
        $sess = $this->getRequest()->getSession();
        $currUsr = $this->User->get($sess->consume('m_id'));
        if( $this->getRequest()->getSession()->check('m_password') ){
            $tabValChange['password'] = $sess->consume('m_password');
        }

        if( $this->getRequest()->getSession()->check('m_email') ){
            $tabValChange['email'] = $sess->consume('m_email');
        }

        if( $this->getRequest()->getSession()->check('m_firstName') ){
            $tabValChange['firstName'] = $sess->consume('m_firstName');
        }

        if( $this->getRequest()->getSession()->check('m_lastName') ){
            $tabValChange['lastName'] = $sess->consume('m_lastName');
        }

        if( $this->getRequest()->getSession()->check('m_status')){
            $tabValChange['status'] = $sess->consume('m_status');
        }

        if( $this->getRequest()->getSession()->check('m_defaultScore')){
            $tabValChange['defaultScore'] = $sess->consume('m_defaultScore');
        }

        if( $this->getRequest()->getSession()->check('m_gender')){
            $tabValChange['gender'] = $sess->consume('m_gender');
        }

        if( $this->getRequest()->getSession()->check('m_size')){
            $tabValChange['size'] = $sess->consume('m_size');
        }

        if( $this->getRequest()->getSession()->check('m_weight')){
            $tabValChange['weight'] = $sess->consume('m_weight');
        }

        if( $this->getRequest()->getSession()->check('m_sizeValue')){
            $tabValChange['sizeValue'] = $sess->consume('m_sizeValue');
        }

        if( $this->getRequest()->getSession()->check('m_weightValue')){
            $tabValChange['weightValue'] = $sess->consume('m_weightValue');
        }

        $this->User->patchEntity($currUsr, $tabValChange);
        if($this->User->save($currUsr)){
            http_response_code(200);
            die();
        }
        else{
            http_response_code(500);
            die();
        }
    }

    // Check the data when someone tries to connect
    function connect(){
        if($this->getRequest()->getSession()->check('m_email')){ // API CALL FROM APP
            $mail = $this->getRequest()->getSession()->consume('m_email');
            $pass = $this->getRequest()->getSession()->consume('m_password');
            $req = $this->User->find()->select(['password'])->where(['email'=>$mail])->first();
            if($req == null || $req['password'] != $pass) http_response_code(403);
            else http_response_code(200);
            echo 'ok';
            die();
        }
        else{ // FROM WEB
            $mail = $this->getRequest()->getData('mail');
            $pass = $this->getRequest()->getData('pass');
            $req = $this->User->find()->select(['id','password','avatar', 'firstName', 'lastName'])->where(['email'=>$mail])->first();

            $this->getRequest()->getSession()->write('id', $req['id']);
            $this->getRequest()->getSession()->write('avatar', $req['avatar']);
            $this->getRequest()->getSession()->write('fullname', $req['firstName']." ".$req['lastName']);
            $message = array();
            if($req == null){
                $message['code'] = 'NOT EXISTING';
            }
            else{
                if($req['password'] != $pass){
                    $message['code'] = "BAD PASSWORD";
                }
                else{
                    $message['code'] = $req['id'];
                }
            }
            $this->set($message);
            $this->set('_serialize', array_keys($message));
        }
    }

    /** WEB PAGE ONLY FUNCTIONS */
    function connexion(){
        $this->redirect('/displaydata');
    }

    function displayData(){
        $id = $this->getRequest()->getSession()->read('id');
        $cookie = $this->getRequest()->getCookie('id');
        if($cookie == null){
            $cookie = (new Cookie('id'))
                ->withValue($id)
                ->withExpiry((new DateTime('+10 year')))
                ->withPath('/')
                ->withDomain('')
                ->withSecure(false)
                ->withHttpOnly(false);
            $this->setResponse(
                $this->getResponse()
                ->withCookie($cookie)
            );
        }
        if($id != null){
            $users = TableRegistry::getTableLocator()->get('User');
            $usr = $users->get($id);
            $this->getRequest()->getSession()->write('mgid', $usr['mainGroupId']);
        }
        else{
            $this->disconnect();
        }
    }

    function disconnect(){
        $this->getRequest()->getSession()->delete('id');
        $this->getRequest()->getSession()->delete('avatar');
        $this->getRequest()->getSession()->delete('mgid');
        $this->getRequest()->getSession()->delete('fullname');
        $cookies = $this->getRequest()->getCookieCollection();
        $cookie = $cookies->get('id');
        $this->setResponse(
            $this->getResponse()
                ->withExpiredCookie($cookie)
        );
        $this->redirect('/');
    }

    function getDataObjectif(){
        $id = $this->getRequest()->getQuery('id');
        $dateDebut = $this->getRequest()->getQuery('dateDebut');
        $date = date('Y-m-d H-i-s');
        $toReturn = $this->User->find()->select()->where(['id'=>$id])->contain('Userstat', function ($q) use($date, $dateDebut){
            return $q
                ->select()
                ->where([
                    'date <='=>$date,
                    'date >='=>$dateDebut
                ]);
        })->first();
        $count = 0;
        foreach($toReturn['userstat'] as $statTemp){
            $count+=$statTemp['count'];
        }
        echo json_encode([$count]);
        die();
    }

    function settings(){
        $id = $this->getRequest()->getSession()->read('id');
        if($id != null){
            $currUsr = $this->User->find()->select()->where(['id'=>$id])->contain(['groupe'])->first();
            $this->set('groups', $currUsr['groupe']);
            $this->set('mgid', $currUsr['mainGroupId']);
        }
        else{
            $this->redirect('/');
        }
    }

    function updateMainGroup(){
        $mgid = $this->getRequest()->getQuery('mgid');
        $usrId = $this->getRequest()->getQuery('uid');
        $users = $this->User;
        $currUsr = $users->find()->select()->where(['id'=>$usrId])->contain(['groupe'])->first();
        $currGrp = $users->Groupe->get($mgid);
        $save = false;
        foreach ($currUsr['groupe'] as $grp){
            if($grp['id'] == $currGrp['id']){
                $save = true;
                break;
            }
        }
        if($save){
            $currUsr->mainGroupId = $mgid;
            if($users->save($currUsr)) echo json_encode(['code'=>'0']);
            else echo json_encode(['code'=>'3']);
        }
        else{
            echo json_encode(['code'=>'6']);
        }
        die();
    }
}