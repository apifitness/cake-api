<?php
    $this->assign('title', 'Nouvel Objectif | API Fitness');
    if(null !== ($this->getRequest()->getSession()->read('error'))){
        echo '<div class="ui negative message" id="errormess">
        <div class="header">
            Il y a eu un problème lors de la création de l\'objectif :(
        </div>
        <p id="errorcontent">'.$this->getRequest()->getSession()->read('error').'</p>
    </div>';
    }
?>

<?= $this->Form->create(null, ['method'=>'POST','class'=>'ui form', 'url'=>['action'=>'sauvegarderNouveau']]); ?>
<?= $this->Form->control('id', ['type'=>'hidden', 'value'=>$this->getRequest()->getSession()->read('id')]) ?>
<div class="field">
    <label>
        Titre de l'objectif :
    </label>
    <input type="text" name="title" placeholder="Titre de l'objectif">
</div>
<div class="field">
    <label>
        Date de début de l'objectif :
    </label>
    <input type="date" name="DateDebut" dataformatas="aaaa-mm-jj">
</div>
<div class="field">
    <label>
        Montant de l'objectif :
    </label>
    <input type="number" name="objectif" placeholder="Montant de l'objectif">
</div>
<button type="submit" class="ui button">
    SAUVEGARDER
</button>
<?= $this->Form->end(); ?>