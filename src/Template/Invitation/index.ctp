<h3>Invitations reçues</h3>
<table class="ui fluid celled striped table">
    <thead>
            <th>Envoyeur</th>
            <th>Nom du Groupe</th>
            <th>Accepter ?</th>
    </thead>
    <tbody id="receivedRequests">

    </tbody>
</table>

<h3>Invitations envoyées <a href="#"><i class="plus icon green" id="addIcon"></i></a></h3>
<table class="ui fluid celled striped table">
    <thead>
        <th>Receveur</th>
        <th>Nom du Groupe</th>
    </thead>
    <tbody id="sentRequests">

    </tbody>
</table>

<script>
    id = "<?= $this->getRequest()->getSession()->read('id'); ?>"
</script>

<?= $this->Html->script('inviteIndex.js'); ?>