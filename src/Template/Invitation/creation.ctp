<div class="ui negative message" id="errormess">
    <i class="close icon"></i>
    <div class="header">
        Impossible de créer l'invitation :(
    </div>
    <p id="errorcontent">Les arguments sont invalides</p>
</div>

<?= $this->Form->create(null, ['class'=>'ui form']) ?>
<div class="field">
    <label>E-Mail de la personne à inviter</label>
    <div class="ui input">
        <input type="text" placeholder="E-Mail" name="mail" id="mailInput">
    </div>
</div>
<div class="field">
    <label>Nom du groupe</label>
    <div class="ui input">
        <input type="text" placeholder="Groupe" name="nom" id="nameInput">
    </div>
</div>
<button type="button" class="ui button" id="btncrea">
    CONFIRMER
</button>
<script>
    id = "<?= $this->getRequest()->getSession()->read('id') ?>";
    var urlString = window.location.href;
    var url = new URL(urlString);
    var gName = url.searchParams.get('gName');
    $('#nameInput').val(gName);
</script>
<?= $this->Form->end() ?>
<?= $this->Html->script('creationInvitation.js') ?>