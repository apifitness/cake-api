<?php

use Cake\Routing\Router; ?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>


    <!-- FETCH CSS FILES : SEMANTIC AND BASE -->
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('utils.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css') ?>

    <!-- FETCH SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>


    <!-- FETCH WEBROOT FILES -->
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    <script type='text/javascript' src="http://wurfl.io/wurfl.js"></script>
    <script>
        baseUrl = encodeURI("<?= Router::url(['controller'=>'User', 'action'=>'test'], true) ?>").split('/user/')[0];
    </script>
    <script>
        id = "<?= $this->getRequest()->getSession()->read('id')?>";
        if(id !== ""){
            $.ajax({
                url:baseUrl+'/userstat/get-curr-week-data',
                data:{
                    id: id
                },
                success: function (data) {
                    $('#stepsTotal').text(data);
                }
            })
        }
    </script>

</head>
<body>
    <?php
    if($this->getRequest()->getSession()->read('id') != null){
        echo '<nav class="ui left vertical inverted sidebar labeled icon menu">';
        echo $this->Html->link($this->Html->image('BFG.png', ['class'=>'ui small image']), 'http://break-first.eu/index.php/fr/innovations/', ['escape'=>false, 'class'=>'header item']);
        echo $this->Html->link('<i class="home icon"></i> Accueil', '/displaydata?id='.$this->getRequest()->getSession()->read('id'), ['class'=>'item', 'escape'=>false]);
        echo $this->Html->link('<i class="users icon"></i> Gestion de mes groupes', '/mygroups', ['class'=>'item', 'escape'=>false]);
        echo $this->Html->link('<i class="cogs icon"></i>Paramètres', '/settings', ['escape'=>false, 'class'=>'item']);
        echo $this->Html->link('<i class="envelope icon"></i> Invitations', '/invites', ['class'=>'item', 'escape'=>false]);
        echo $this->Html->link('<i class="sign out icon"></i>Déconnexion', '/disconnect', ['escape'=>false, 'class'=>'item']);
        echo '</nav>';
    }
    ?>
    <?= $this->Flash->render() ?>
    <div class="pusher">
        <div class="ui container" id="site" >
            <?= $this->Html->script('sha256.js') ?>
            <?= $this->Html->script('utils.js') ?>
            <div class="ui fluid grid">
                <div class="row">
                    <div class="three wide column">
                        <?php
                        if($this->getRequest()->getSession()->check('id')){
                            echo '<button type="button" class="sbTrigger circular ui fluid black icon button"><i class="white bars icon center"></i></button>
                          ';
                        }
                        ?>
                    </div>
                    <div class="ten wide column">
                        <?php
                            echo '<h1 class="title center">WE WALK</h1>';
                        ?>
                        <?= $this->fetch('content') ?>
                    </div>
                    <div class="computer only three wide column">
                        <div style="position: relative; left: 75%;">
                            <div class="inline">
                                <?php
                                if($this->getRequest()->getSession()->check('fullname')){
                                    echo '<h2>'.$this->getRequest()->getSession()->read('fullname').'</h2>';
                                }
                                if($this->getRequest()->getSession()->check('avatar')){
                                    echo '<img src="'.$this->getRequest()->getSession()->read('avatar').'" class="ui small image" alt="avatar utilisateur" />';
                                }
                                if($this->getRequest()->getSession()->check('id')){
                                    echo '<h3>Nombre de pas total : <span id="stepsTotal"></span></h3>';
                                }
                                ?>

                            </div>
                        </div>
                        <div id="popupMsg">

                        </div>
                        <script>
                            let id = "<?= $this->getRequest()->getSession()->read('id'); ?>";
                            if(id !== ""){
                                $.ajax({
                                    url:baseUrl+'/invitation/count-by-id',
                                    data: {
                                        id: id
                                    },
                                    success:function (data) {
                                        console.log(parseInt(data));
                                        if(parseInt(data)){
                                            let html = (data === "1") ? '<div class="ui card"><div class="content">Vous avez '+data+' invitation en attente.</div></div>' : '<div class="ui card"><div class="content">Vous avez '+data+' invitations en attente.</div></div>';
                                            $('#popupMsg').append(html);
                                        }
                                    }
                                })
                            }

                        </script>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <footer>
    </footer>
</body>
</html>
