<?php
$this->assign('title', 'Mes Groupes | API Fitness');
?>

<table class="ui striped table">
    <thead>
        <tr>
            <th>Les groupes dont vous êtes membre</th>
        </tr>
    </thead>
    <tbody id="groupTable">
    </tbody>
</table>

<button>Créer un nouveau groupe</button>
<br/>
<div style="/*display:none">
    <form>
        <div class="ui input" data-content="Nom du groupe">
            <input name="nom" placeholder="Nom du groupe" >
        </div><br/>
        <div class="ui input">
            <input placeholder="Invité 1" name="InviteUn">
        </div><br/>
        <div class="ui input">
            <input placeholder="Invité 2" name="InviteDeux">
        </div><br/>
        <div class="ui input">
            <input placeholder="Invité 3" name="InviteTrois">
        </div><br/>
        <div class="ui input">
            <input placeholder="Invité 1" name="InviteQuatre">
        </div><br/>
        <div class="ui input">
            <input placeholder="Invité 5" name="InviteCinq">
        </div><br/>
    </form>
</div>

<script>
    id= "<?= $this->getRequest()->getSession()->read('id') ?>";
</script>

<?=
    $this->Html->script('groupIndex.js');
?>