<p>Tous les changements effectués sont automatiquement sauvegardés</p>

<div class="ui raised segment">
    <label>Sélectionnez votre groupe principal : </label>
    <br/>
    <div class="ui selection dropdown" id="groupDropdown">
        <input type="hidden" name="gender">
        <i class="dropdown icon"></i>
        <div class="default text">―</div>
        <div class="menu">
            <?php
                foreach ($groups as $grp){
                    echo '<div class="item" data-value="'.$grp['id'].'">'.$grp['nom'].'</div>';
                }
            ?>
        </div>
    </div>
</div>

<?= $this->Html->script('settings.js') ?>

<script>
    $('.ui.dropdown').dropdown();
    id = "<?= $this->getRequest()->getSession()->read('id'); ?>";
    tempMgid = "<?= $mgid ?>;"
</script>