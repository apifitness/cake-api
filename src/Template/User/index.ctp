<?= $this->Html->image('BFG.png') ?>
<?php
    $this->assign('title', 'Accueil | API Fitness')
?>
    <div class="ui negative message" id="errormess">
        <i class="close icon"></i>
        <div class="header">
            Il y a eu un problème lors de la connexion :(
        </div>
        <p id="errorcontent"></p>
    </div>
    <div class="ui success message" id="succmessage">
        <i class="close icon"></i>
        <div class="header">
            La connexion a réussi ! :)
        </div>
    </div>
    <div class="ui negative message" id="badmess">
        <i class="close icon"></i>
        <div class="header">
            Il y a eu un problème lors de l'inscription :(
        </div>
        <p id="errmess"></p>
    </div>
    <div class="ui success message" id="goodmess">
        <i class="close icon"></i>
        <div class="header">
            L'inscription a réussi ! :)
        </div>
    </div>
<div id="thirdPart">
    <div id="thirdPartU">
        <div class="ui attached message">
            <div class="header">
                Bon retour parmi nous !
            </div>
            <p>Je vous prie de bien vouloir remplir ce formulaire de connexion :)</p>
        </div>
        <form class="ui form attached segment">
            <div class="field">
                <div class="ui input">
                    <input name="email" type="email" placeholder="Votre adresse e-mail" id="mailU">
                </div>
            </div>
            <div class="field">
                <div class="ui input">
                    <input name="pass" type="password" placeholder="Votre mot de passe" id="passU">
                </div>
            </div>
            <button class="ui button" id="connectU">CONNEXION</button>
            <div class="ui dimmer" id="dimmerU"></div>
            <p>Mot de passe oublié ? <a href=# id="clickMDPRESET">cliquez ici</a></p>
        </form>
        <div class="ui bottom attached warning message">
            <i class="icon help"></i>
            Pas encore de compte ? Dans ce cas inscrivez-vous <a href="#" id="toInscr">ici</a> !
        </div>
    </div>
    <!-- INSCRIPTION -->
    <div id="thirdPartUI">
        <div class="ui attached message">
            <div class="header">
                Bievenue !
            </div>
            <p>Je vous prie de bien vouloir remplir ce formulaire d'inscription :)</p>
        </div>
        <?= $this->Form->create(null, ['class'=>'ui form attached segment', 'type'=>'file', 'url'=>'/register', 'id'=>'formReg']) ?>
        <p><span class="red">*</span> Champs obligatoires</p>
            <div class="three fields">
                <div class="field">
                    <div class="ui input">
                        <label class="red">*</label>
                        <input name="firstName" type="text" maxlength="50" placeholder="Votre/vos prénom(s)" id="fnU">
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <label class="red">*</label>
                        <input name="lastName" type="text" maxlength="50" placeholder="Votre nom de famille" id="lnU">
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input name="gender" type="text" maxlength="1" placeholder="Votre genre" id="genU">
                    </div>
                </div>
            </div>
            <div class="four fields">
                <div class="field">
                    <div class="ui input">
                        <input name="weightValue" type="number" placeholder="Votre poids" id="wU" maxlength="3">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid selection dropdown" id="weight_dropdown">
                        <i class="ui dropdown icon"></i>
                        <div class="default text">Type</div>
                        <div class="menu" id="weight_choice">
                            <div class="item" data-value="KG">KG</div>
                            <div class="item" data-value="LBS">LBS</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input name="heightValue" type="number" placeholder="Votre taille" id="hU" maxlength="4">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid selection dropdown" id="height_dropdown">
                        <i class="ui dropdown icon"></i>
                        <div class="default text">Type</div>
                        <div class="menu" id="height_choice">
                            <div class="item" data-value="KG">CM</div>
                            <div class="item" data-value="LBS">INCHES</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Votre date de naissance (AAAA-MM-dd HH:mm:ss) : </label>
                <input name="birthdate" class="ui input" id="inputDate">
                <div class="ui fluid popup top left transition hidden">
                    <div id="head">
                        <button type="button" id="prev" class="ui white button">⬅</button>
                        <h3 id="currMonth"></h3>
                        <button type="button" id="next" class="ui white button">➡</button>
                        <div class="ui selection dropdown" id="yearsdrop">
                            <i class="dropdown icon"></i>
                            <div class="default text" >Année</div>
                            <div class="menu" id="years">
                            </div>
                        </div>
                    </div>
                    <table id="calendar">
                        <colgroup span="7"></colgroup>
                        <tr>

                        </tr>
                        <tr>
                            <td class="day">
                                Lun
                            </td>
                            <td class="day">
                                Mar
                            </td>
                            <td class="day">
                                Mer
                            </td>
                            <td class="day">
                                Jeu
                            </td>
                            <td class="day">
                                Ven
                            </td>
                            <td class="day">
                                Sam
                            </td>
                            <td class="day">
                                Dim
                            </td>
                        </tr>
                        <tr id="line1">

                        </tr>
                        <tr id="line2">

                        </tr>
                        <tr id="line3">

                        </tr>
                        <tr id="line4">

                        </tr>
                        <tr id="line5">

                        </tr>
                        <tr id="line6">

                        </tr>
                    </table>
                    <p id="displayDate"></p>
                </div>
            </div>
            <div class="field">
                <label>Identifiants de connexion : <span class="red">*</span></label>
                <div class="ui input">
                    <input name="email" type="email" placeholder="Votre adresse e-mail" id="mailUI">
                </div>
            </div>
            <div class="field">
                <div class="ui input">
                    <input name="password" type="password" placeholder="Votre mot de passe" id="passUI">
                </div>
            </div>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" name="wristband" id="isWristband">
                    <label>Je possède un bracelet !</label>
                </div>
                    <input type="text" name="wristbandId" placeholder="identifiant du bracelet" id="wristbandId">
            </div>
        <div class="field">
            <label>Votre avatar</label>
            <input type="file" name="avatar" accept="image/jpeg, image/png" id="file">
        </div>
            <button class="ui button" id="inscription" type="submit">INSCRIPTION</button>
            <div class="ui dimmer" id="dimmerUI"></div>
        <?= $this->Form->end(); ?>
        <div class="ui bottom attached warning message">
            Vous avez déjà un compte ? Dans ce cas connectez-vous <a href="#" id="toConn">ici</a> !
        </div>
    </div>

</div>
    <div class="ui basic modal">
        <div class="header">
            <i class="envelope icon"></i><br/>
            <h4>Un mail vous a été envoyé</h4>
        </div>
        <div class="content">
            Veuillez suivre les instructions envoyées à votre adresse email pour modifier votre mot de passe.
        </div>
    </div>

<?php
echo $this->Html->script('userIndex');
echo $this->Html->script('/calendar/script.js');
echo $this->Html->css('/calendar/calendar.css');
    ?>