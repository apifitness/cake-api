<?php $avatar = $avatar[0]; ?>
<div class="ui grid">
    <div class="row">
        <div class="three wide column">
            <div id="avatarcontainer" class="ui fluid images">
                <?= $this->Html->image('skinTone'.$avatar['skinToneId'].".png", ['class'=>'superposable', 'id'=>'skinTone']) ?>
                <?= $this->Html->image('hat'.$avatar['hatId'].".png", ['class'=>'superposable', 'id'=>'hat']) ?>
                <?= $this->Html->image('eyesColor'.$avatar['eyesColorId'].".png", ['class'=>'superposable', 'id'=>'eyesColor']) ?>
                <?= $this->Html->image('glasses'.$avatar['glassesId'].".png", ['class'=>'superposable', 'id'=>'glasses']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <h3>Théo</h3></div>
</div>
<script>
    $('#skinTone').css('z-index', '0');
    $('#eyesColor').css('z-index', '1');
    $('#glasses').css('z-index', '2');
    $('#hat').css('z-index', '100');

    /**
     * https://jsfiddle.net/7vL8pqyf/5/
     * */
</script>

<?= $this->Html->css('avatar.css'); ?>