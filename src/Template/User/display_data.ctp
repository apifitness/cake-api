<div class="ui active dimmer">
    <div class="ui indeterminate text loader">Chargement des données en cours...</div>
</div>
<div class="computer only ui two column grid">
    <div class="row" style="display: none;">
    </div>
    <h4 class="ui horizontal divider header">
        Vos données
    </h4>
    <div class="row">
        <div class="column">
            <div class="inline center">
                <button type="button" class="ui teal button icon circular prevWeek" ><i class="white angle left icon"></i></button>
                <p>Week <span class="weekCurr"></span></p>
                <button type="button" class="ui teal button icon circular nextWeek"><i class="white angle right icon"></i></button>
            </div>
            <div id="objWeek" style="height: 300px; width: 100%;"></div>
        </div>
        <div class="column">
            <div class="inline center">
                <button type="button" class="ui teal button icon circular prevMonth"><i class="white angle left icon"></i></button>
                <p>Month <span class="monthCurr"></span></p>
                <button type="button" class="ui teal button icon circular nextMonth"><i class="white angle right icon"></i></button>
            </div>
            <div id="objMonth" style="height: 300px; width: 100%;"></div>
        </div>
    </div>
    <h4 class="ui horizontal divider header darkgray">
        Données du groupe <span id="gName"></span>
    </h4>
    <div class="row">
        <div class="column">
            <div id="groupChartWeekComputer" style="height: 300px; width: 100%;"></div>
            <div id="groupImagesWeeklyComputer" class="ui flex-container images"></div>
        </div>
        <div class="column">
            <div id="groupChartMonthComputer" style="height: 300px; width: 100%;"></div>
            <div id="groupImagesMonthlyComputer" class="ui flex-container images"></div>
        </div>
    </div>
</div>
<div class="tablet only mobile only ui grid">
    <div class="ui top attached tabular menu" style="width:100vw">
        <div class="item active" data-tab="weekly">
            Données Hebdomadaires
        </div>
        <div class="item" data-tab="monthly">
            Données Mensuelles
        </div>
    </div>
    <div class="ui tab active" data-tab="weekly">
        <div class="row">
            <div class="inline center">
                <button type="button" class="ui teal button icon circular prevWeek" ><i class="white angle left icon"></i></button>
                <p>Week <span class="weekCurr"></span></p>
                <button type="button" class="ui teal button icon circular nextWeek"><i class="white angle right icon"></i></button>
            </div>
            <div id="chartWeekMobile" style="width: 90vw; height: 30vh;"></div>
        </div>
        <div class="row">
            <div id="chartWeekGroupMobile" style="width: 90vw; height: 30vh;"></div>
            <div id="groupImagesWeeklyMobile" class="ui flex-container images"></div>
        </div>
    </div>
    <div class="ui tab" data-tab="monthly">
        <div class="inline center">
            <button type="button" class="ui teal button icon circular prevMonth"><i class="white angle left icon"></i></button>
            <p>Month <span class="monthCurr"></span></p>
            <button type="button" class="ui teal button icon circular nextMonth"><i class="white angle right icon"></i></button>
        </div>
        <div class="row">
            <div id="chartMonthMobile" style="width: 90vw; height: 30vh;"></div>
        </div>
        <div class="row">
            <div id="chartMonthGroupMobile" style="width: 90vw; height: 30vh;"></div>
            <div id="groupImagesMonthlyMobile" class="ui flex-container images"></div>
        </div>
    </div>
</div>





<script>
    id = "<?= $this->getRequest()->getSession()->read('id') ?>";
    mgid = "<?= $this->getRequest()->getSession()->read('mgid'); ?>";
</script>
<?= $this->Html->script('ttt.js') ?>